# Script de gestion des données  en base
# Les fonctions proposées dans ce script sont customisées en fonction des besoins
#

# Dans ce fichier on part du principe qu'on travaille avec des fichiers nommés:
# A_B_C_D_nnn.tif

# Fonction de création de la table de synthèse
# A minima, on doit y retrouver les champs: geom, image, cat, surf, surf_pg, area_etalon, duree  (NE PAS MODIFIER LES LIGNES CORRESPONDANTES)
# Les autres champs, sont ceux issus de variables internes au script, ou calculés à partir du nom du fichier de scan
#     i.e : individu, type, date, esp
# TYPAGE: chaque nom de champ est suivi du type postgre à utiliser (integer, numeric, character varying(x) , ...)
function F_create_table_synthese() {
 db.execute --quiet "CREATE TABLE $SCHEMA.$SCHEMA
(
    geom public.geometry,
    image character varying(255),
    cat integer,
    surf numeric,
    surf_pg numeric,
    area_etalon numeric,
    duree numeric,
    dep character varying(20),
    esp character varying(20),
    ech character varying(5),
    petiole character varying(20),
    manip character varying(5),
    prel character varying(5),
    date character varying(10)
)"
 db.execute --quiet "GRANT SELECT ON TABLE $SCHEMA.$SCHEMA TO foret_apache"
}

# Fonction pour alimenter la table de synthèse
# Elle intègre les données de l'image en cours de traitement dans la table de synthèse
# Les variables rendues disponibles par le script principal sont:
#      $file: nom du fichier tif correspondant au scan en cours de traitement
#      $DUREE: durée des calculs pour le scan en cours"
#      $AREA_ETALON: surface de l'étalon calculée pour le scan en cours"

function F_alimente_table_synthese() {
  IMAGE=$(basename $file)
  dep=$(echo $IMAGE |  awk -F'_' '{print $1}')
  esp=$(echo $IMAGE |  awk -F'_' '{print $2}')
  ech=$(echo $IMAGE |  awk -F'_' '{print $3}')
  petiole=$(echo $IMAGE |  awk -F'_' '{print $4}')
  manip=$(echo $IMAGE |  awk -F'_' '{print $5}')
  prel=$(echo $IMAGE |  awk -F'_' '{print $6}')
  date=$(echo $IMAGE |  awk -F'_' '{print $7}')
  echo "INFO: valeurs à intégrer en base: image=$IMAGE dep=$dep esp=$esp ech=$ech petiole=$petiole =$manip prel=$prel date=$date"
  db.execute --quiet sql="INSERT INTO $SCHEMA.$SCHEMA(image, cat, surf, dep, esp, ech, petiole, manip, prel, date, geom,area_etalon, duree, surf_pg)  select image,cat, surf,'$dep','$esp','$ech','$petiole','$manip','$prel','$date', wkb_geometry, area_etalon,duree, public.ST_Area(wkb_geometry::public.geometry)/$COEFF_ETALON from ${SCHEMA}.$NOM_TABLE "

}

# Fonction pour créer une vue simplifiant l'affichage
function F_create_view() {
# création de la  vue de synthèse dans le schéma public
db.execute --quiet " CREATE OR REPLACE VIEW $SCHEMA.${SCHEMA}_view AS SELECT $SCHEMA.image as images, sum($SCHEMA.surf_pg) AS surf_indiv, \
$SCHEMA.dep, $SCHEMA.esp, $SCHEMA.ech, $SCHEMA.petiole , $SCHEMA.manip \
FROM $SCHEMA.$SCHEMA  GROUP BY $SCHEMA.image, $SCHEMA.dep, $SCHEMA.esp, $SCHEMA.ech, $SCHEMA.petiole, $SCHEMA.manip"


# attribution des droits à foret_apache pour exploitation via l'application Rshiny
db.execute --quiet "GRANT SELECT ON TABLE $SCHEMA.${SCHEMA}_view TO foret_apache"
}
