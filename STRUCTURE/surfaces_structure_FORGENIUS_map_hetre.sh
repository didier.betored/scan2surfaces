# Script de gestion des données  en base
# Les fonctions proposées dans ce script peuvent être partiellement customisées en fonction des besoins
#


# Fonction de création de la table de synthèse
# A minima, on doit y retrouver les champs: geom, image, cat, surf, surf_pg  (NE PAS MODIFIER LES LIGNES CORRESPONDANTES)
# Les autres champs, sont ceux issus de variables internes au script, ou calculés à partir du nom du fichier de scan
#     i.e : individu, type, date, espece
# TYPAGE: chaque nom de champ est suivi du type postgre à utiliser (integer, numeric, character varying(x) , ...)
function F_create_table_synthese() {
 db.execute --quiet "CREATE TABLE $SCHEMA.$SCHEMA
(
    geom public.geometry,
    image character varying(255),
    cat integer,
    surf numeric,
    pays_im character varying(20),
    pays_dir character varying(20),
    espece character varying(20),
    gcu character varying(5),
    individu character varying(5),
    type character varying(10),
    manip character varying(5),
    area_etalon numeric,
    duree numeric,
    surf_pg numeric
)"
 db.execute --quiet "GRANT SELECT ON TABLE $SCHEMA.$SCHEMA TO foret_apache"
}

# Fonction pour alimenter la table de synthèse
# Elle intègre les données de l'image en cours de traitement dans la table de synthèse
# Pour les variables calculées par la fonction (TYPE, ESPECE, INDIVIDU,DATE), il est prudent de les valider avant le lancement du script ...
# Les variables rendues disponibles par le script principal sont:
#      $file: nom du fichier tif correspondant au scan en cours de traitement
#      $DUREE: durée des calculs pour le scan en cours"
#      $AREA_ETALON: surface de l'étalon calculée pour le scan en cours"

function F_alimente_table_synthese() {
# Pour FORGENIUS, la convention des noms de fichiers: <pays sur 2 carac.>_<type><num individu>
# A revoir
  IMAGE=$(basename $file)
  PAYS_IM=$(echo $IMAGE |  awk -F'_' '{print $1}')
  ESPECE=$(echo $IMAGE |  awk -F'_' '{print $2}')
  GCU=$(echo $IMAGE |  awk -F'_' '{print $3}')
  INDIVIDU=$(echo $IMAGE |  awk -F'_' '{print $4}')
  TYPE=$(echo $SCAN_DIR |  awk -F'/' '{print $4}' )  # FORGENIUS/PV/Espagne/feuille  --> renvoie feuille
  MANIP=$(echo $SCAN_DIR | awk -F'/' '{print $2}')   # FORGENIUS/PV/Espagne/feuille  --> renvoie PV
  PAYS_DIR=$(echo $SCAN_DIR | awk -F'/' '{print $3}')   # FORGENIUS/PV/Espagne/feuille  --> renvoie Espagne
  echo "INFO: valeurs à intégrer en base: image=$IMAGE pays=$PAYS_IM espèce=$ESPECE gcu=$GCU individu=$INDIVIDU"
  db.execute --quiet sql="INSERT INTO $SCHEMA.$SCHEMA(image, cat, surf, pays_im, espece, gcu, individu, type, manip, pays_dir,geom,area_etalon, duree, surf_pg) select image,cat, surf,'$PAYS_IM','$ESPECE','$GCU','$INDIVIDU','$TYPE','$MANIP','$PAYS_DIR',wkb_geometry, area_etalon,duree, public.ST_Area(wkb_geometry::public.geometry)/$COEFF_ETALON from ${SCHEMA}.$NOM_TABLE "

}

# Fonction pour créer une vue simplifiant l'affichage
function F_create_view() {
# création de la  vue dans le schéma public
db.execute --quiet "CREATE OR REPLACE VIEW $SCHEMA.${SCHEMA}_view AS SELECT string_agg($SCHEMA.image,’,’), sum($SCHEMA.surf) AS surf_indiv, \
$SCHEMA.pays_im, $SCHEMA.pays_dir, $SCHEMA.manip, \
$SCHEMA.type, $SCHEMA.espece, $SCHEMA.individu \
FROM $SCHEMA.$SCHEMA  GROUP BY $SCHEMA.type, $SCHEMA.espece, $SCHEMA.individu, $SCHEMA.pays_im, \
$SCHEMA.pays_dir, $SCHEMA.manip"

# attribution des droits à foret_apache
db.execute --quiet "GRANT SELECT ON TABLE $SCHEMA.${SCHEMA}_view TO foret_apache"
}
