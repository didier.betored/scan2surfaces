# Script de gestion des données  en base
# Les fonctions proposées dans ce script sont partiellement customisées en fonction des besoins
# Toujours partir du fichier générique (entre autres pour y trouver plus de commentaires que dans les fichiers adaptés

function F_create_table_synthese() {
 db.execute --quiet "CREATE TABLE $SCHEMA.$SCHEMA
(
    geom public.geometry,
    image character varying(255),
    cat integer,
    surf numeric,
    surf_pg numeric,
    area_etalon numeric,
    duree numeric,
    dispo character varying(20),
    espece character varying(20),
    prov character varying(20),
    ind character varying(5),
   manip character varying(5),
   nb numeric
)"

 db.execute --quiet "GRANT SELECT ON TABLE $SCHEMA.$SCHEMA TO foret_apache"
}

function F_alimente_table_synthese() {
  IMAGE=$(basename $file)
  DISPO=$(echo $IMAGE |  awk -F'_' '{print $1}')
  ESPECE=$(echo $IMAGE |  awk -F'_' '{print $2}')
  PROV=$(echo $IMAGE |  awk -F'_' '{print $3}')
  IND=$(echo $IMAGE |  awk -F'_' '{print $4}')
  MANIP=$(echo $IMAGE |  awk -F'_' '{print $5}')
  NB=$(echo $IMAGE |  awk -F'_' '{print $6}')
  echo "INFO: valeurs à intégrer en base: image=$IMAGE dispo=$DISPO espece=$ESPECE prov=$PROV ind=$IND manip=$MANIP nb=$NB"
  db.execute --quiet sql="INSERT INTO $SCHEMA.$SCHEMA(image, cat, surf, dispo, espece, prov, ind, manip, nb, geom, area_etalon, duree, surf_pg)  select image, cat, surf, '$DISPO', '$ESPECE', '$PROV', '$IND', '$MANIP', '$NB',  wkb_geometry, area_etalon, duree, public.ST_Area(wkb_geometry::public.geometry)/$COEFF_ETALON from ${SCHEMA}.$NOM_TABLE "
}

# Fonction pour créer une vue simplifiant l'affichage
function F_create_view() {
# création de la  vue de synthèse dans le schéma public
db.execute --quiet " CREATE OR REPLACE VIEW $SCHEMA.${SCHEMA}_view AS SELECT string_agg($SCHEMA.image,','), sum($SCHEMA.surf_pg) AS surf_indiv, \
$SCHEMA.dispo, $SCHEMA.espece, $SCHEMA.prov, $SCHEMA.ind, $SCHEMA.manip
FROM $SCHEMA.$SCHEMA  GROUP BY $SCHEMA.dispo, $SCHEMA.espece, $SCHEMA.prov, $SCHEMA.ind, $SCHEMA.manip"


# attribution des droits à foret_apache
db.execute --quiet "GRANT SELECT ON TABLE $SCHEMA.${SCHEMA}_view TO foret_apache"
}
