# Script de gestion des données  en base
# Les fonctions proposées dans ce script peuvent être partiellement customisées en fonction des besoins 
#



# Fonction de création de la table de synthèse
# A minima, on doit y retrouver les champs: geom, image, cat, surf, surf_pg  (NE PAS MODIFIER LES LIGNES CORRESPONDANTES)
# Les autres champs, sont ceux issus de variables internes au script, ou calculés à partir du nom du fichier de scan
#     i.e : individu, type, date, espece
# TYPAGE: chaque nom de champ est suivi du type postgre à utiliser (integer, numeric, character varying(x) , ...)
function F_create_table_synthese() {
  db.execute --quiet "CREATE TABLE $SCHEMA.$SCHEMA
(
    geom public.geometry,
    image character varying(255),
    cat integer,
    surf numeric,
    type character varying(10),
    date character varying(8),
    espece character varying(20),
    individu character varying(5),
    placette character varying(5),
    area_etalon numeric,
    duree numeric,
    surf_pg numeric
)"
}

# Fonction pour alimenter la table de synthèse
# Elle intègre les données de l'image en cours de traitement dans la table de synthèse
# Pour les variables calculées par la fonction (TYPE, ESPECE, INDIVIDU,DATE), il est prudent de les valider avant le lancement du script ...
# Les variables rendues disponibles par le script principal sont:
#      $file: nom du fichier tif correspondant au scan en cours de traitement
#      $DUREE: durée des calculs pour le scan en cours"
#      $AREA_ETALON: surface de l'étalon calculée pour le scan en cours"
function F_alimente_table_synthese() {
# Pour FontBlanche, la convention des noms de fichiers: FR_FBn_<date sur 8 caractères>_<espèce sur 1 caractère><num individu>
IMAGE=$(basename $file)
TYPE='feuille'
ESPECE=$(echo $IMAGE |  awk -F'_' '{print $5}'| sed -e 's/[0-9][0-9]*//' -e 's/.tif//') # c007 devient c
INDIVIDU=$(echo $IMAGE |  awk -F'_' '{print $5}' | sed -e 's/[a-z]//' -e 's/.tif//')  # c007 devient 007
PLACETTE=$(echo $IMAGE | awk -F'_' '{print $4}')
DATE=$(echo $IMAGE |  awk -F'_' '{print $3}')

echo "INFO: valeurs à intégrer en base: image=$IMAGE espèce=$ESPECE individu=$INDIVIDU placette=$PLACETTE date=$DATE, durée=$DUREE, surf_étalon=$AREA_ETALON"
db.execute --quiet sql="INSERT INTO $SCHEMA.$SCHEMA(image, cat,surf, espece, individu, placette, type, date, geom, duree,area_etalon,surf_pg) select image,cat,surf,'$ESPECE','$INDIVIDU','$PLACETTE','$TYPE','$DATE', wkb_geometry,duree,area_etalon,public.st_area(wkb_geometry::public.geometry)/$COEFF_ETALON from ${SCHEMA}.$NOM_TABLE "

}


# Fonction pour créer une vue simplifiant l'affichage function F_create_view() 
function F_create_view() {
# création de la  vue de synthèse dans le schéma public
db.execute --quiet " CREATE OR REPLACE VIEW $SCHEMA.${SCHEMA}_view AS SELECT $SCHEMA.image as images, sum($SCHEMA.surf_pg) AS surf_indiv, \
$SCHEMA.type, $SCHEMA.espece, $SCHEMA.individu , $SCHEMA.placette \
FROM $SCHEMA.$SCHEMA  GROUP BY $SCHEMA.image, $SCHEMA.individu, $SCHEMA.espece,$SCHEMA.type, $SCHEMA.placette"


# attribution des droits à foret_apache pour exploitation via l'application Rshiny
db.execute --quiet "GRANT SELECT ON TABLE $SCHEMA.${SCHEMA}_view TO foret_apache"
}

