# Script de gestion des données  en base
# Les fonctions proposées dans ce script sont customisées en fonction des besoins
#

# Dans ce fichier on part du principe qu'on travaille avec des fichiers nommés:
# A_B_C_D_nnn.tif

# Fonction de création de la table de synthèse
# A minima, on doit y retrouver les champs: geom, image, cat, surf, surf_pg, area_etalon, duree  (NE PAS MODIFIER LES LIGNES CORRESPONDANTES)
# Les autres champs, sont ceux issus de variables internes au script, ou calculés à partir du nom du fichier de scan
#     i.e : individu, type, date, espece
# TYPAGE: chaque nom de champ est suivi du type postgre à utiliser (integer, numeric, character varying(x) , ...)
function F_create_table_synthese() {
 db.execute --quiet "CREATE TABLE $SCHEMA.$SCHEMA
(
    geom public.geometry,
    image character varying(255),
    cat integer,
    surf numeric,
    surf_pg numeric,
    area_etalon numeric,
    duree numeric,
    a character varying(20),
    b character varying(20),
    c character varying(20),
    d character varying(5)
)"
 db.execute --quiet "GRANT SELECT ON TABLE $SCHEMA.$SCHEMA TO foret_apache"
}

# Fonction pour alimenter la table de synthèse
# Elle intègre les données de l'image en cours de traitement dans la table de synthèse
# Les variables rendues disponibles par le script principal sont:
#      $file: nom du fichier tif correspondant au scan en cours de traitement
#      $DUREE: durée des calculs pour le scan en cours"
#      $AREA_ETALON: surface de l'étalon calculée pour le scan en cours"

function F_alimente_table_synthese() {
  IMAGE=$(basename $file)
  a=$(echo $IMAGE |  awk -F'_' '{print $1}')
  b=$(echo $IMAGE |  awk -F'_' '{print $2}')
  c=$(echo $IMAGE |  awk -F'_' '{print $3}')
  d=$(echo $IMAGE |  awk -F'_' '{print $4}')
  echo "INFO: valeurs à intégrer en base: image=$IMAGE a=$a b=$b c=$c d=$d"
  db.execute --quiet sql="INSERT INTO $SCHEMA.$SCHEMA(image, cat, surf, a, b, c, d, geom,area_etalon, duree, surf_pg)  select image,cat, surf,'$a','$b','$c','$c','$d',wkb_geometry, area_etalon,duree, public.ST_Area(wkb_geometry::public.geometry)/$COEFF_ETALON from ${SCHEMA}.$NOM_TABLE "

}

# Fonction pour créer une vue simplifiant l'affichage
function F_create_view() {
# création de la  vue de synthèse dans le schéma public
db.execute --quiet " CREATE OR REPLACE VIEW $SCHEMA.${SCHEMA}_view AS SELECT string_agg($SCHEMA.image,',') as images, sum($SCHEMA.surf_pg) AS surf_indiv, \
$SCHEMA.a, $SCHEMA.b, $SCHEMA.c, $SCHEMA.d \
FROM $SCHEMA.$SCHEMA  GROUP BY $SCHEMA.a, $SCHEMA.b, $SCHEMA.c, $SCHEMA.d"


# attribution des droits à foret_apache pour exploitation via l'application Rshiny
db.execute --quiet "GRANT SELECT ON TABLE $SCHEMA.${SCHEMA}_view TO foret_apache"
}
