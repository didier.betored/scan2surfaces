# Script de gestion des données  en base
# Les fonctions proposées dans ce script peuvent être partiellement customisées en fonction des besoins 
#



# Fonction de création de la table de synthèse
# A minima, on doit y retrouver les champs: geom, image, cat, surf, surf_pg  (NE PAS MODIFIER LES LIGNES CORRESPONDANTES)
# Les autres champs, sont ceux issus de variables internes au script, ou calculés à partir du nom du fichier de scan
#     i.e : individu, type, date, espece
# TYPAGE: chaque nom de champ est suivi du type postgre à utiliser (integer, numeric, character varying(x) , ...)
function F_create_table_synthese() {
  db.execute --quiet "CREATE TABLE cormier.cormier
(
    geom public.geometry,
    image character varying(255),
    site character varying(5),
    clone character varying(10),
    placette character varying(5),
    ramet character varying(5),
    nb_folio numeric,
    area numeric,
    surf numeric,
    surf_qgis_corr numeric,
    surf_pg_px numeric,
    surf_pg_mm numeric,
    pilosite boolean,
    rouge numeric,
    vert numeric,
    bleu numeric,
    nb_dent_d numeric,
    nb_dent_g numeric,
    num_feuil numeric,
    posit_feuil character varying(1),
    abimee boolean,
    soudee boolean,
    longueur numeric,
    l_10 numeric,
    l_25 numeric,
    l_50 numeric,
    l_75 numeric,
    l_90 numeric
)"

  db.execute --quiet "GRANT SELECT ON TABLE cormier.cormier TO foret_apache;"
}

# Fonction pour alimenter la table de synthèse
# Elle intègre les données de l'image en cours de traitement dans la table de synthèse
# Pour les variables calculées par la fonction (TYPE, ESPECE, INDIVIDU,DATE), il est prudent de les valider avant le lancement du script ...
# Les variables rendues disponibles par le script principal sont:
#      $file: nom du fichier tif correspondant au scan en cours de traitement
#      $DUREE: durée des calculs pour le scan en cours"
#      $AREA_ETALON: surface de l'étalon calculée pour le scan en cours"
function F_alimente_table_synthese() {
# Pour FontBlanche, la convention des noms de fichiers: FR_FBn_<date sur 8 caractères>_<espèce sur 1 caractère><num individu>
IMAGE=$(basename $file)
SITE=$(echo $IMAGE |  awk -F'_' '{print $1}' | sed -e 's/[a-z]//' -e 's/.tif//')
CLONE=$(echo $IMAGE |  awk -F'_' '{print $2}' | sed -e 's/[a-z]//' -e 's/.tif//')
PLACETTE=$(echo $IMAGE |  awk -F'_' '{print $3}' | sed -e 's/[a-z]//' -e 's/.tif//')
RAMET=$(echo $IMAGE |  awk -F'_' '{print $4}' | sed -e 's/[a-z]//' -e 's/.tif//')
NB_FOLIO=$(echo $IMAGE |  awk -F'_' '{print $5}' | sed -e 's/[a-z]//' -e 's/.tif//')

echo "INFO: valeurs à intégrer en base: image=$IMAGE ... "
db.execute --quiet sql="INSERT INTO cormier.cormier (image, site, clone, placette, ramet, nb_folio, geom, area, surf, surf_qgis_corr, surf_pg_px, surf_pg_mm, pilosite, rouge, vert, bleu, nb_dent_d, nb_dent_g, num_feuil, posit_feuil, abimee, soudee, longueur, l_10, l_25, l_50, l_75, l_90) SELECT image, '$SITE', '$CLONE', '$PLACETTE', '$RAMET' ,'$NB_FOLIO' ,wkb_geometry ,area ,surf ,surf_qgis_corr ,public.ST_area(wkb_geometry), public.ST_area(wkb_geometry::public.geometry)/$COEFF_ETALON, pilosite, rouge, vert, bleu, nb_dent_d, nb_dent_g, num_feuil, posit_feuil, abimee, soudee, longueur/23.593, public.ST_Length(larg_calc_10)/23.593, public.ST_Length(larg_calc_25)/23.593, public.ST_Length(larg_calc_50)/23.593, public.ST_Length(larg_calc_75)/23.593, public.ST_Length(larg_calc_90)/23.593 FROM ${SCHEMA}.$NOM_TABLE "

}

# Fonction pour créer une vue simplifiant l'affichage
function F_create_view() {
# création de la  vue dans le schéma public
db.execute --quiet "CREATE OR REPLACE VIEW $SCHEMA.${SCHEMA}_view AS SELECT string_agg($SCHEMA.image,',') AS images, \
sum($SCHEMA.surf) AS surf_indiv, \
$SCHEMA.pays_im, $SCHEMA.manip, \
$SCHEMA.espece, $SCHEMA.individu \
FROM $SCHEMA.$SCHEMA  GROUP BY $SCHEMA.espece, $SCHEMA.individu, $SCHEMA.pays_im, $SCHEMA.manip"

# attribution des droits à foret_apache
db.execute --quiet "GRANT SELECT ON TABLE cormier.cormier_view TO foret_apache"
}
