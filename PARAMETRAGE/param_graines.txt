#
#  Fichier des paramètres de l'application
#  Lisez la documentation avant d'intervenir dans ce fichier
#

# MAPSET     : Espace GRASS dans lequel il stocke ses données.
# SEUIL_SURFACE_P2 : Permet d'éliminer des surfaces (trop petites pour avoir un sens ); exprimé en pixel
# SEUIL_SURFACE_MM2 : Permet d'éliminer des surfaces (trop petites pour avoir un sens ); exprimé en mm2
#          ces deux paramètres ne sont pas mobilisés au même moment dans le script
# POIDS_BLEU  : coefficient pour ajuster l'identification des surfaces de végéta (piffométrique: 2.95 pour chène guillaume, 3.1 fonctionne sur le reste)
# DEBUGGREGION: Défini la zone du scan à analyser pour les surfaces de végétal en mode debug: une surface restreinte pour accélérer les calculs
# BUFF:  paramètre qui controle l'opération de dilatation-érosion afin de lisser les contours des feuilles
MAPSET=cypres_cp
SEUIL_SURFACE_P2=200
SEUIL_SURFACE_MM2=0.1
DEBUGGREGION="n=6970 s=20 e=7700 w=20"
POIDS_BLEU=3.1
BUFF=5

# pour les graines
# CROISSANCE: coef de dilation pour la fonction growth
# SEUIL_SURFACE: seuil de surface mini (en dessous, les polygones sont supprimés)
# LISSAGE: coef de lissage
CROISSANCE=2
SEUIL_SURFACE= 70
LISSAGE= 3
