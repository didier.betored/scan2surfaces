
#  Fichier des paramètres de l'application
#  Lisez la documentation avant d'intervenir dans ce fichier
#

# MAPSET     : Espace GRASS dans lequel il stocke ses données.
# SEUIL_SURFACE_P2 : Permet d'éliminer des surfaces (trop petites pour avoir un sens ); exprimé en pixel
# SEUIL_SURFACE_MM2 : Permet d'éliminer des surfaces (trop petites pour avoir un sens ); exprimé en mm2
#          ces deux paramètres ne sont pas mobilisés au même moment dans le script
# POIDS_BLEU  : coefficient pour ajuster l'identification des surfaces de végéta (piffométrique: 2.95 pour chène guillaume, 3.1 fonctionne sur le reste)
# DEBUGGREGION: Défini la zone du scan à analyser pour les surfaces de végétal en mode debug: une surface restreinte pour accélérer les calculs
# BUFF:  paramètre qui controle l'opération de dilatation-érosion afin de lisser les contours des feuilles
# EROSION: paramètre d'érosion suite à la dilation effectuée avec BUFF. Peut être supérieur à la dilatation
# MIN_C, MAX_C, STEP_C: permet de construire un vecteur de seuil à tester pour l'algo C (utilisation de la composition colorée plutôt que couleur par couleur)
#                       Les valeurs usuelles: MIN_C=23400 MAX_C=24200  STEP_C=200
#                       Attention, au plus on agrandit cette plage (nombre d'occurence) au plus ce sera long !
MAPSET=PERMANENT
SEUIL_SURFACE_P2=100
SEUIL_SURFACE_MM2=2
DEBUGGREGION="n=6970 s=20 e=7700 w=20"
POIDS_BLEU=2.8
BUFF=5
EROSION=8
MIN_C=23000
MAX_C=23600
STEP_C=100

# pour les graines
# CROISSANCE: coef de dilation pour la fonction growth
# SEUIL_SURFACE: seuil de surface mini (en dessous, les polygones sont supprimés)
# LISSAGE: coef de lissage
CROISSANCE=2
SEUIL_SURFACE= 70
LISSAGE= 3

