#
#  Fichier des paramètres de l'application
#  Lisez la documentation avant d'intervenir dans ce fichier
#
# USER       : utilisateur postgres qui créera les tables de sortie 
# PGPASSWORD : mot de passe postgresde l'utilisateur
# MAPSET     : Espace GRASS dans lequel il stocke ses données.
# SEUIL_SURFACE_P2 : Permet d'éliminer des surfaces (trop petites pour avoir un sens ); exprimé en pixel
# SEUIL_SURFACE_MM2 : Permet d'éliminer des surfaces (trop petites pour avoir un sens ); exprimé en mm2
#          ces deux paramètres ne sont pas mobilisés au même moment dans le script
# POIDS_BLEU  : coefficient pour ajuster l'identification des surfaces de végéta (piffométrique: 2.95 pour chène guillaume, 3.1 fonctionne sur le reste)
# DEBUGGREGION: Défini la zone du scan à analyser pour les surfaces de végétal en mode debug: une surface restreinte pour accélérer les calculs
# BUFF:  paramètre qui controle l'opération de dilatation-érosion afin de lisser les contours des feuilles
USER=dbetored
PGPASSWORD=Audyle
MAPSET=forgenius
SEUIL_SURFACE_P2=100
SEUIL_SURFACE_MM2=3
DEBUGGREGION="n=5000 s=4200 e=1650 w=450"
POIDS_BLEU=3.1
BUFF=3
