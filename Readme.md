# Script "surface"

Ce script est utilisé pour calculer des surfaces de feuille/tige à partir d'images scannées

Développé en bash (shell unix), il utilise des commandes du Système d'Information Géographique Grass pour déterminer les surfaces des objets scannés.

# Principes généraux

Les fichiers scannés sont stockés dans des dossiers thématiques, avec une convention de nommage permettant de retrouver certaines informations qui serviront à organiser les résultats en sortie.

Les sorties sont organisées dans une base de données postgresql, dans une table de synthèse, elle même enregistrée dans un schéma dont le nom est le même que le dossier thématique. Cette table de synthèse comporte donc autant de ligne que d'objets (feuilles ou tiges) détectés dans les différents fichiers scan trouvés dans le dossier thématique.

Chaque scan fait l'objet de la génération d'un table dans ce même schéma, à des fins de traçabilité.

Les fonctionnalités de grass permettent d'obtenir des géométries vectorielles accessibles depuis la base de données postgres

Les algorithmes utilisés ne sont pas parfaits ! Il convient de visualiser les résultats et d'éventuellement retoucher les formes obtenues pour affiner les résultats.

Lorsqu'il y a plusieurs individus sur un même scan, toutes les commandes de post traitement (synthèse en particulier), doivent se faire en manuel.

Le pilotage des valeurs de sorties se fait au moyen de quelques fonctions définies dans un fichier nommé surfaces_structure_<projet>.sh, qui demande à être customisé en fonction des besoins du projet. Cf chapitre "Données en sortie"

# Contraintes
Les fichiers de scan doivent avoir des noms conventionnés si on souhaite pouvoir exploiter des fonctions d'extraction de variables à partir de ce nom. Ces variables pourront donc compléter les variables proposées dans la table de synthèse

Les noms des fichiers de scan ne doivent pas comporter d'accent ni de caractères spéciaux (espace ou autres ...)

L'application grass doit être lancée avant le lancement du script (un contrôle est effectué au démarrage du script)


# Composition

* surfaces.sh: script principal, pilotant l'ensemble des opérations
* surfaces_tools.sh: ensemble de fonctions utilitaires, dont la lecture des arguments d'appel, et le chargement de la structure de sortie souhaitée
* surfaces_structure_*.sh: fichiers spécifiques aux projets, avec une définition des variables de sorties demandées
* surfaces_constantes.txt: contient les valeurs dites constantes nécessaires pour le fonctionnement du script
* param*: contient les paramètres spécifiques pour une utilisation. 

# Obtenir le code
Le code est sur la forge MIA dans un projet public
La commande ci-dessous permet de le récupérer (GIT étant forcément déjà installé sur la machine car GRASS étant obligatoire, le serveur est imposé)

```bash
 git clone git@forgemia.inra.fr:didier.betored/scan2surfaces.git
```

# Utilisation

Le script s'utilise à partir d'une session grass préalablement démarrée. Le script a été testé et validé dans un environnement grass78.


Il faut ensuite créer le fichier pour la structure de la sortie, en utilisant l'un des exemples fournis (surfaces_structure_*.sh) et en le modifiant pour l'adapter à ses besoins. (cf chapitre "Données en sortie")

L'utilisateur est tout d'abord invité à créer un fichier de paramètre spécifique pour son cas d'utilisation en partant du modèle parametres_surf.txt  (cf chapitre paramétrage)

Ceci se fait avec la commande: 
```bash
./surfaces.sh -g mes_parametres.txt
```
Ensuite, il faut éditer le fichier obtenu (mes_parametres.txt) pour adapter les valeurs des paramètres principaux.
Et on lance le traitement avec la commande:

```bash
./surfaces.sh -f mes_parametres.txt -S FBn -D dir1/dir2
```


Dans cet exemple, on lance le script avec le fichier de paramètre nommé "mes_parametres.txt". 
On indique également la structure de sortie désirés avec l'option "-S FBn". 
Cette option va charger les fonctions adaptées au projet "FontBlanche", à partir du contenu du script surfaces_structure_FBn.sh

Et enfin, on désigne le dossier (dir1) et sous dossier(dir2) dans lequel sont les scans.
A noter que ces répertoires sont relatifs à la constante BASE_DIR_SCAN définie dans le fichier surfaces_constantes.txt


*Sans aucun paramètre, le message affiché indique quelles sont les options disponibles*
================================================================================================
```
USAGE: ./surfaces.sh -g <nom_fichier.txt>
USAGE: ./surfaces.sh -f <parametres.txt> -S <nom_structure>  -D <dir1|dir1/dir2> -algo [M|C|G]  [-fo <jpg|tif|cr2>] [-so] [-b] [-d] [-e] [-ne] [-m] [-v] [-stat]  [-U <nom_fichier_image.tif>] [ -X <image>] [-no]
 
Script de calcul de surfaces foliaires pour un dossier de scans
Ce script peut aussi être utilisé pour analyser des photos de graines
Paramètres:
    -f <parametres.txt>: (obligatoire) nom du fichier contenant les paramètres. Ce fichier est décrit ...
    -S <nom structure>: (obligatoire) désigne le type de structuration de la table de données en sortie. Lié également à la construction des noms des fichiers scan
                        Les structures connues: FBn, FORGENIUS, FORGENIUS_PM_methodo
                        En cas de besoin de définir une nouvelle structure, merci de contacter un administrateur
    -D <dir1|dir1/dir2>: (obligatoire) désigne un répertoire, ou un chemin, à partir du chemin désigné par la constante BASE_DIR_SCAN: 
                Le nom du schéma dans la base est calculé à partir de cet argument, dans lequel on remplace les / par des _
                Le nombre de sous-répertoires n'est pas limité. Il faut par contre respecter la consigne: aucun caractères spéciaux, accentués ou espace

    -g <nom fichier de paramètres>: (optionnel) génère un fichier de paramètres avec les valeurs par défaut

    -algo [M|C|G] : choix d'un algorithme de traitement. Si non précisé, l'agorithme utilisé est celui pour les feuilles vertes (algorithme historique basé sur la recherche de la couleur verte)
          M: (mono) algorithme par défaut si non précisé. Filtre basé sur l'exploitation canal par canal pour identifier le vert et les ombres
          C: (composition) algorithme avec optimisation automatique d'un paramètre basé sur l'utilisation de la composition colorée.
          G: (graines) algorithme avec optimisation automatique d'un paramètre, à mobiliser pour les scans de graines
    -fo <jpg|tif|cr2>: (optionnel) choix du format des images sources. cr2 correspondant au format raw. Par défaut, ce sont les images avec l'extension .tif qui sont recherchées.
    -b : (optionnel) à activer si les images ne contiennent aucune couleur bleue (nécroses)
    -d : (optionnel) active le mode debug (une seule image traitée, sur une zone réduite)
    -ne : (optionnel) désactive le controle de l'étalon (non codé) avant le calcul des surfaces
    -e : (optionnel) force le script à n'effectuer que le controle des étalons.Rien n'est stocké en base
    -m : (optionnel) empêche le ménage en fin de script. Activé par défaut en mode debug
    -v : (optionnel) force l'affichage de certaines couches pendant les calculs. Forcé en mode debug
    -U <nom_fichier_image.tif>: (optionnel) ne traitera que l'image dont le nom de fichier est donné
    -X <image>: (optionnel) permet d'empêcher le traitement de l'image dont le nom est fourni. 
                 Cette option peut être précisée plusieurs fois pour exclure plusieurs images
    -no: (optionnel) empêche la suppression du schéma de stockage des résultats, et ne traite que les images non intégrées dans la table de synthèse
    ============================
    FONCTIONS DE POST-TRAITEMENT
    ============================
    Les fonctionnalités suivantes sont à lancer après la génération des tables élémentaires, et surtout, après la vérification visuelle
    des résultats obtenus. En général, cette vérification conduit malgré tout à modifier les contours des feuilles.

    Attention: Les fonctions suivantes ne peuvent pas être combinées avec les options -U (image unique), ou  -X (exclusion)

    -so: permet de générer la table de synthèse en base. Les surfaces des contours corrigés seront recalculées
         Doit être lancée avec les mêmes arguments que lors de l'appel qui à généré les tables élémentaires.
    -stat: génère les calculs des statistiques sur les images. 
         Doit être lancée avec les mêmes arguments que lors de l'appel qui à généré les tables élémentaires.
    -archive: permet de générer une archive contenant toutes les données associées à un projet de calcul de surfaces
         Le projet est désigné par les arguments fichier de paramètre, Fichier de structure et dossier contenant les scans
         Doit être positionné en dernier dans la liste des arguments
    -clean: permet de supprimer l'ensemble des données associées à un projet de calcul de surfaces
         Le projet est désigné par les arguments fichier de paramètre, Fichier de structure et dossier contenant les scans
         Doit être positionné en dernier dans la liste des arguments
    -history: montre l'historique associé à un projet de calcul de surfaces
         Le projet est désigné par les arguments fichier de paramètre, Fichier de structure et dossier contenant les scans
         Doit être positionné en dernier dans la liste des arguments

++++++++++++++++++++++
Exploitation standard 
++++++++++++++++++++++
./surfaces.sh -g parametres_projet.txt  --> pour créer le fichier de paramètres par défaut et l'éditer pour l'adapter au cas d'usage
./surfaces.sh -f parametres_projet.txt -S FORGENIUS -D FORGENIUS/PV/Espagne/feuille  --> pour lancer l'analyse des scans et générer les tables de surfaces par scan

A partir de là, il faut vérifier sous Qgis, si les contours obtenus automatiquement sont corrects, et le cas échéant les adapter au mieux
./surfaces.sh -f parametres_projet.txt -S FORGENIUS -D FORGENIUS/PV/Espagne/feuille -so   --> pour générer la table de synthèse et la vue d'exploitation finale
./surfaces.sh -f parametres_projet.txt -S FORGENIUS -D FORGENIUS/PV/Espagne/feuille -stat   --> pour générer la table des statistiques des bandes rouge, vert et bleu par contour.

A tout moment, on peut demander l'affichage des traitements effectués pour une structure
En effet, dans la base de  données, on garde la trace de qui lance quoi sur quelles données ...
./surfaces.sh -f parametres_projet.txt -S FORGENIUS -D FORGENIUS/PV/Espagne/feuille -history   --> pour afficher la liste des commandes lancées sur le projet de surface désignés par les arguments.

# Ensuite, pour archiver, puis nettoyer les données:
./surfaces.sh -f parametres_projet.txt -S FORGENIUS -D FORGENIUS/PV/Espagne/feuille -archive   --> pour générer une archive avec toutes les données entrées et sorties
./surfaces.sh -f parametres_projet.txt -S FORGENIUS -D FORGENIUS/PV/Espagne/feuille -clean   --> pour nettoyer les données dans grass et postgre. Nécessaire pour faire de la place!
++++++++++++++++++++++

# Ensuite, pour archiver, puis nettoyer les données:
./surfaces.sh -f parametres_projet.txt -S FORGENIUS -D FORGENIUS/PV/Espagne/feuille -archive   --> pour générer une archive avec toutes les données entrées et sorties
./surfaces.sh -f parametres_projet.txt -S FORGENIUS -D FORGENIUS/PV/Espagne/feuille -clean   --> pour nettoyer les données dans grass et postgre. Nécessaire pour faire de la place!
++++++++++++++++++++++
```
# Paramétrage
L'exemple proposé ci-dessous est celui du fichier parametres_surf.txt

Les variables à adapter prioritairement au cas d'utilisation sont principalement:
* SCHEMA
* MAPSET

Utilisez les définitions proposées en début de fichier pour comprendre leur signification et leur impact sur le fonctionnement du script
```
# USER       : utilisateur postgres qui est en charge des surfaces, qui est fixe maintenant (surfacier)
# SCHEMA     : Schéma dans la base où les tables sont stockées. Tenter de faire correspondre le schéma au nom du répertoire de stockage des scans
# SEUIL_SURFACE_P2 : Permet d'éliminer des surfaces (trop petites pour avoir un sens ); exprimé en pixel
# SEUIL_SURFACE_MM2 : Permet d'éliminer des surfaces (trop petites pour avoir un sens ); exprimé en mm2
#          ces deux paramètres ne sont pas mobilisés au même moment dans le script
# MAPSET     : "répertoire" GRASS où les étapes d'analyse se font.
# PGPASSWORD : mot de passe postgres de l'utilisateur "surfacier" défini plus haut
# POIDS_BLEU  : coefficient pour ajuster l'identification des surfaces de végéta (piffométrique: 2.95 pour chène guillaume, 3.1 fonctionne sur le reste)
# DEBUGGREGION: Défini la zone du scan à analyser pour les surfaces de végétal en mode debug: une surface restreinte pour accélérer les calculs
# BUFF:  paramètre qui controle l'opération de dilatation-érosion 
USER=dbetored
SCHEMA=fb_icos
PGPASSWORD=unsecure_password
MAPSET=FontBlanche
SEUIL_SURFACE_P2=750
SEUIL_SURFACE_MM2=3
DEBUGGREGION="n=6970 s=20 e=7700 w=20"
POIDS_BLEU=3.1
BUFF=50
```

# Données en sortie (Structure)

De manière générique, dans le sous script de génération des données en sortie, on doit trouver 3 fonctions:
* F_ajout_attributs: qui créé les attributs minimum nécessaires dans la structure de sortie
* F_create_table_synthese: qui créé la table de sortie, vide
* F_alimente_table_synthese: fonction appelée à la fin du traitement de chaque image pour peupler la table de synthèse.
* F_create_view: fonction appelée à la toute fin de l'exécution du script pour créer la vue de synthèse par individu.
Les fonctions "F_create_table_synthese" et "F_alimente_table_synthese" nécessitent une intervention de l'utilisateur pour adapter les sorties.

## F_create_table_synthese

Fonction de création de la table de synthèse
A minima, on doit y retrouver les champs: geom, image, cat, surf, surf_pg  (NE PAS MODIFIER LES LIGNES CORRESPONDANTES)
Les autres champs, sont ceux issus de variables internes au script, ou calculés à partir du nom du fichier de scan ( i.e : individu, type, date, espece)

TYPAGE: chaque nom de champ est suivi du type postgre à utiliser (integer, numeric, character varying(x) , ...)

Ainsi, le contenu minimum de cette fonction doit être le suivant:

```bash
function F_create_table_synthese() {
  db.execute --quiet "CREATE TABLE $SCHEMA.$SCHEMA
(
    geom public.geometry,
    image character varying(255),
    cat integer,
    surf numeric,
    surf_pg numeric
)"
}

```
En lisant ce code relativement simple, on comprend qu'une table de synthèse est créée dans le schéma dont le nom correspond au dossier thématique (cf chapitre "Principes généraux)
et le nom de la table prendra la même valeur que le nom du schéma

Et cette table comporte 5 variables (geom, image, cat, surf, surf_pg)

Rajouter une variable, c'est rajouter une ligne contenant un nom de variable et un type postgre:

exemple: date character varying(8)  --> indique à postgre de créer une variable date qui sera une chaine de caractères codée sur 8 caractères

## F_alimente_table_synthèse

Fonction pour alimenter la table de synthèse. Elle est appelée en toute fin de traitement, lorsque les tables par scan ont été validées individuellement.

Elle intègre les données de chaque scan dans la table de synthèse

Pour les variables calculées par la fonction (TYPE, ESPECE, INDIVIDU,DATE), il est prudent de les valider avant le lancement du script ...

Ainsi, à titre d'exemple, si dans cette fonction on écrit
```bash
ESPECE=$(echo $IMAGE |  awk -F'_' '{print $4}'| sed -e 's/[0-9][0-9]*//' -e 's/.tif//')
```
Il faut l'évaluer à l'invite de commande avec les commandes suivantes:
```bash
IMAGE=nom_fichier_image_c0001.tif
ESPECE=$(echo $IMAGE |  awk -F'_' '{print $4}'| sed -e 's/[0-9][0-9]*//' -e 's/.tif//')
echo $ESPECE
```
* La première instruction défini un nom de fichier image
* la deuxième est la commande à évaluer
* la troisième affiche le résultat: Si on n'obtient pas la lettre "c", c'est qu'il y a un problème dans l'écriture des enchainements de commandes.

Les variables rendues disponibles par le script principal sont:
*      $file: nom du fichier tif correspondant au scan en cours de traitement
*      $DUREE: durée des calculs pour le scan en cours"
*      $AREA_ETALON: surface de l'étalon calculée pour le scan en cours"


La commande d'écriture dans la table de synthèse ressemble à:
```bash
db.execute --quiet sql="INSERT INTO $SCHEMA.$SCHEMA(image, cat,surf, espece, individu, type, date, geom, duree,etalon) select image,cat,surf,'$ESPECE','$INDIVIDU','$TYPE','$DATE', wkb_geometry,'$DUREE','$AREA_ETALON' from ${SCHEMA}.$NOM_TABLE "
```
La première série de champ (indiquée entre parenthèses) indique les champs qu'on souhaite mettre à jour dans la table de synthèse. A priori, tous ceux qui sont définis par la fonction F_create_table_synthese.

Dans l'exemple, la liste est donc: image, cat,surf, espece, individu, type, date, geom, duree,etalon

La deuxième série désine les valeurs effectives qui vont alimenter les champs. On doit les fournir dans le bon ordre !

Dans l'exemple, la liste est donc:  image,cat,surf,'$ESPECE','$INDIVIDU','$TYPE','$DATE', wkb_geometry,'$DUREE','$AREA_ETALON'. 
On repère ici qu'il y a des valeurs qui proviennent de la table temporaire (dédiée à l'image en cours de traitement) et d'autres valeurs, entourés de guillements simples, qui sont des variables du script, calculées juste avant dans la fonction, on bien des valeurs fournies par le script principal pour la durée et la surface de l'étalon.

**Ici, il s'agit de bien respecter la syntaxe pour éviter les erreurs à l'exécution, que ce soit dans le bash ou dans le SQL**

