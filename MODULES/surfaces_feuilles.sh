# Fonction de traitement pour le calcul des surfaces des feuilles
# Appelée par surfaces.sh, lorsque le mode feuille est actif (option -algo C)
# ce n'est pas l'algorithme par défaut.

function F_algo_feuilles() {

# Forçage de l'érosion pour les fichiers de paramètres qui ne contiennent pas érosion
if [ -z "$EROSION" ]; then
   EROSION=$BUFF
fi

# mémorisation instant de début du script
debut=$(date +%s)

# détermination automatique du seuil de bleu optimum pour attraper les pixels verts
min_growth=1000
min_seuil_bleu=1
previous_area=1

# Ssb: version du scan sans les blancs
r.mapcalc --quiet --overwrite expression=Ssb="if($NOM_SCAN@$MAPSET > 32500,0 ,$NOM_SCAN@$MAPSET)"

#arrSeuils=( 23000 23500 23600 23700 23800 24000 24200 )

# Création du vecteur des seuils à partir des paramètres
arrSeuils=($(seq $MIN_C $STEP_C $MAX_C))
somme=0
premier=1  # flag pour ignorer le premier accroissement 
for seuil in  "${arrSeuils[@]}"
do
  r.mapcalc --quiet --overwrite expression=Sb="if(Ssb > $seuil,null(),1)"
  r.grow --quiet --overwrite input=Sb output=Seuilplus5 radius=5
  r.grow --quiet --overwrite input=seuilplus5 output=S5euilmoins radius=-7

  surface_seuil=$(r.stats --quiet -c input=S5euilmoins | grep '1 ' | awk -F" " '{print $2}' )
  growth=$(bc <<< "1000000 * ($surface_seuil - $previous_area) / $previous_area")

  arrGrowth[${#arrGrowth[@]}]=$growth
  arrSurface[${#arrSurface[@]}]=$surface_seuil
  # pour ne pas prendre le premier accroissement qui est abérent
  if [ "$premier" == "0" ]; then
    somme=$(bc  <<< "$somme + $growth")
  else
    premier=0
  fi
  echo "INFO: seuil=$seuil surface=$surface_seuil acroissement: $growth somme=$somme"
  previous_area=$surface_seuil
done

# calcul moyenne des accroissements
moyenne=$((somme  / ${#arrGrowth[@]}))

# recherche du premier minimum dans la série growth
for i in  {1..11..1}
do
  if [ ${arrGrowth[$i]} -lt $moyenne ]; then
    indice_seuil=$i
    break
  fi
done

min_seuil=${arrSeuils[$indice_seuil]}
echo "INFO: seuil retenu: $min_seuil, indice $indice_seuil"

r.mapcalc --quiet --overwrite expression=S01="if(Ssb > $seuil,null(),Ssb)"
r.mapcalc --quiet --overwrite expression=S02="if(Ssb<1,null(),1)"
r.mapcalc --quiet --overwrite expression=S12=S01*S02
r.mapcalc --quiet --overwrite expression=S="if(isnull(S12),1,0)"

echo "INFO: entoure les pixels contigus"
r.contour --quiet --overwrite input=S@$MAPSET output=scan_contour levels=0.5 cut=$SEUIL_SURFACE_P2

v.category --quiet --overwrite input=scan_contour@$MAPSET output=S2@$MAPSET option=del cat=-1
v.category --quiet --overwrite input=S2@$MAPSET layer=1 type=point,line,centroid,face output=scan_contour@$MAPSET option=add cat=1 step=1

echo "INFO: nettoyages ..."
v.clean  --overwrite input=scan_contour@$MAPSET output=scan_contour_elague_1@$MAPSET tool=prune thres=1 type=line

echo "INFO: nettoyages suite ..."
v.generalize --overwrite --verbose input=scan_contour_elague_1@$MAPSET type=line output=scan_contour_gene@$MAPSET method=douglas threshold=2.5

echo "INFO: transforme les lignes en boundaries (frontières)"
v.type --quiet --overwrite input=scan_contour_gene@$MAPSET output=scan_bound from_type=line to_type=boundary

if [ $DISPLAY_GRASS == 1 ]; then
    echo "INFO: Affichage de scan_bound"
    d.vect --quiet map=scan_bound@$MAPSET color=yellow  fill_color=blue width=1 attribute_column=cat
fi

echo "INFO: Sélection des objets de type boundary (scan_extract)"
v.extract --overwrite input=scan_bound@$MAPSET layer=1 type=boundary output=scan_extract new=-1



echo "INFO: ajout des centroides pour faire des surfaces"
v.centroids --overwrite input=scan_extract@$MAPSET output=scan_surf

if [ $DISPLAY_GRASS == 1 ]; then
    echo "INFO: Affichage de scan_surf"
    d.vect --quiet map=scan_surf@$MAPSET color=red fill_color=red width=1 attribute_column=cat
fi

echo "INFO: export vers shapefile"
v.out.ogr --quiet --overwrite input=scan_surf@$MAPSET output=${REP_OUTPUT}/scan_surf_$$.shp format=ESRI_Shapefile

echo "INFO: import du shapefile"
v.in.ogr --quiet --overwrite input=${REP_OUTPUT}/scan_surf_$$.shp output=scan_imp

if [ $DEBUG == 1 ]; then
    echo "INFO: Affichage de scan_imp"
    d.vect --quiet map=scan_imp@$MAPSET color=green  fill_color=blue width=1 attribute_column=cat
fi

echo "INFO: Nettoyage des petites surfaces (< $SEUIL_SURFACE_P2)"
v.clean --quiet input=scan_imp@$MAPSET output=scan_surf2 tool=rmarea thres=$SEUIL_SURFACE_P2 type=point,line,area --overwrite

if [ $DEBUG == 1 ]; then
    echo "INFO: Affichage de scan_surf2"
    d.vect --quiet map=scan_surf2@$MAPSET color=blue  fill_color=blue width=1 attribute_column=cat
fi

echo "INFO: Suppression des catégories d'origine"
if [ $DEBUG == 1 ]; then
  echo "v.category --overwrite input=scan_surf2@$MAPSET output=scan_cat_del option=del cat=-1"
fi
v.category --quiet --overwrite input=scan_surf2@$MAPSET output=scan_cat_del option=del cat=-1

if [ $DEBUG == 1 ]; then
    echo "INFO: Affichage de scan_cat_del"
    d.vect --quiet map=scan_cat_del@$MAPSET color=orange  fill_color=blue width=1 attribute_column=cat
fi

echo "INFO: Création d'un buffer de $BUFF pixels (en positif)"
if [ $DEBUG == 1 ]; then
  echo "v.buffer --quiet  input=scan_cat_add@$MAPSET type=area output=scan_buff_plus@$MAPSET distance=$BUFF --overwrite"
fi
v.buffer --quiet -t input=scan_cat_del@$MAPSET type=area output=scan_buff_plus@$MAPSET distance=$BUFF --overwrite

echo "INFO: Création d'une valeur de cat constante pour pouvoir fusionner les aires au contact"
if [ $DEBUG == 1 ]; then
  echo "v.category --overwrite input=scan_buff_plus@$MAPSET output=scan_buff_cat_fix@$MAPSET option=add cat=2 step=0"
fi
v.category --quiet --overwrite input=scan_buff_plus@$MAPSET output=scan_buff_cat_fix@$MAPSET option=add cat=2 step=0

echo "INFO: Création d'un vecteur fusionnant les aires au contact"
if [ $DEBUG == 1 ]; then
  echo "v.dissolve --overwrite input=scan_buff_cat_fix@$MAPSET column=cat output=scan_fus"
fi
v.dissolve --quiet --overwrite input=scan_buff_cat_fix@$MAPSET output=scan_fus column=cat


echo "INFO: Création d'un buffer de $BUFF pixels (en négatif) pour revenir aux limites originelles"
if [ $DEBUG == 1 ]; then
  echo "v.buffer --quiet  input=scan_fus@$MAPSET type=area output=scan_buff_moins@$MAPSET distance=-$EROSION --overwrite"
fi
v.buffer --quiet -t input=scan_fus@$MAPSET type=area output=scan_buff_moins@$MAPSET distance=-$EROSION  --overwrite

echo "INFO: Suppression des catégories constantes"
if [ $DEBUG == 1 ]; then
  echo "v.category --overwrite input=scan_buff_moins@$MAPSET output=scan_cat_del2@$MAPSET option=del cat=-1"
fi
v.category --quiet --overwrite input=scan_buff_moins@$MAPSET output=scan_cat_del2@$MAPSET option=del cat=-1

echo "INFO: Création des nouvelles catégories"
if [ $DEBUG == 1 ]; then
  echo "v.category --overwrite input=scan_cat_del2@$MAPSET layer=1 type=point,line,centroid,face output=scan_cat_add@$MAPSET option=add cat=1 step=1"
fi
v.category --quiet --overwrite input=scan_cat_del2@$MAPSET layer=1 type=point,line,centroid,face output=scan_cat_add@$MAPSET option=add cat=1 step=1

echo "INFO: reconstruction de la topologie"
if [ $DEBUG == 1 ]; then
  echo "v.build --overwrite --quiet map=scan_cat_add@$MAPSET"
fi
v.build --overwrite --quiet map=scan_cat_add@$MAPSET

if [ $DEBUG == 1 ]; then
    echo "INFO: Affichage du vecteur scan_cat_add"
    d.vect --quiet map=scan_cat_add@$MAPSET color=green  fill_color=blue width=1 attribute_column=cat
fi

# création d'un table pour la map scan_cat_add
v.db.addtable map=scan_cat_add@$MAPSET layer=1 --quiet

#Ajout attribut area
v.to.db --quiet map=scan_cat_add@$MAPSET option=area columns=area  --overwrite

if [ $DEBUG == 1 ]; then
    echo "INFO: Affichage du vecteur final"
    d.vect --quiet map=scan_cat_add@$MAPSET color=yellow  fill_color=blue width=1 attribute_column=cat
fi
echo "INFO: Ajout des attributs obligatoires (image, surf)"
v.db.addcolumn --quiet map=scan_cat_add@$MAPSET columns="image varchar(80)"
v.db.addcolumn --quiet map=scan_cat_add@$MAPSET columns="surf double precision"
v.db.addcolumn --quiet map=scan_cat_add@$MAPSET columns="area_etalon integer"
v.db.addcolumn --quiet map=scan_cat_add@$MAPSET columns="duree integer"

echo "INFO: export de la couche vecteur en base de données (avec la géométrie)"

#if [ $DEBUG == 1 ]; then
#  echo "INFO COMMANDE: v.out.ogr -u input=scan_cat_add@$MAPSET type=area output="PG:host=$BDDHOST dbname=$BDD user=$USER_BDD password=$PGPASSWORD" output_layer=${SCHEMA}.${NOM_TABLE}"
#fi
#v.out.ogr --quiet -u input=scan_cat_add@$MAPSET type=area output="PG:host=$BDDHOST dbname=$BDD user=$USER_BDD password=$PGPASSWORD" output_layer=${SCHEMA}.${NOM_TABLE} layer=1 format=P>

if [ $DEBUG == 1 ]; then
  echo "INFO COMMANDE: v.out.postgis --overwrite input="scan_cat_add@$MAPSET" output="PG:host=$BDDHOST dbname=$BDD user=$USER_BDD password=$PGPASSWORD" output_layer=${SCHEMA}.${NOM_TABLE}"
fi

g.copy vector=scan_cat_add@$MAPSET,$NOM_TABLE --overwrite
v.out.postgis --overwrite input="scan_cat_add@$MAPSET" output="PG:host=$BDDHOST dbname=$BDD user=$USER_BDD password=$PGPASSWORD" output_layer=${SCHEMA}.${NOM_TABLE}


F_connect_to_db_pgsql

echo "INFO: conversion pixel2 vers mm2 dans la table $NOM_TABLE"
db.execute --quiet sql="UPDATE ${SCHEMA}.$NOM_TABLE SET image = '$(basename $file)', surf = round(CAST( area/$COEFF_ETALON as numeric),2)"

echo "INFO:  attribution des droits à foret_apache sur la table $NOM_TABLE"
db.execute --quiet sql="GRANT ALL ON ${SCHEMA}.$NOM_TABLE TO foret_apache;"

echo "INFO: Suppression des petites surfaces (< $SEUIL_SURFACE_MM2) de la table $NOM_TABLE"
db.execute --quiet sql="DELETE FROM  ${SCHEMA}.${NOM_TABLE} WHERE surf < $SEUIL_SURFACE_MM2"

echo "INFO: export de la couche vecteur en base de données (avec la géométrie)"

# calcul durée du calcul pour l'image en cours
DUREE=$((($(date +%s) - $debut)/60))

# mise à jour des champs area_etalon et durée pour la table intermédiaire
PGPASSWORD=$PGPASSWORD psql -qtA -h $BDDHOST -U $USER_BDD -d $BDD -c "UPDATE $SCHEMA.$NOM_TABLE SET (area_etalon, duree) = ($AREA_ETALON, $DUREE) " 2>&1

#if [ $DEBUG == 1 ]; then
# echo "INFO COMMANDE: v.out.postgis --overwrite input="scan_cat_add@$MAPSET" output="PG:host=$BDDHOST dbname=$BDD user=$USER_BDD password=$PGPASSWORD" output_layer=${SCHEMA}.${NOM_TABLE}"
#fi
#g.copy vector=scan_cat_add@$MAPSET,$NOM_TABLE --overwrite
#v.out.postgis --overwrite input="scan_cat_add@$MAPSET" output="PG:host=$BDDHOST dbname=$BDD user=$USER_BDD password=$PGPASSWORD" output_layer=${SCHEMA}.${NOM_TABLE}
#v.out.ogr --quiet -u input=scan_cat_add@$MAPSET type=area output="PG:host=$BDDHOST dbname=$BDD user=$USER_BDD password=$PGPASSWORD" output_layer=${SCHEMA}.${NOM_TABLE} layer=1 format=P>



}
