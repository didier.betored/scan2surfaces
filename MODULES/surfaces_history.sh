#!/bin/bash
#
#  Script de calcul de surface foliaires
#  Gestion de la traçabilité des exécutions du script scan2surfaces
#

# Variables générales
TABLE_HISTORY=public.history_scan2surfaces   # désigne la table pour stocker la traçabilité des exécutions du script

# Fonction destinée à créer la table history_scan2surfaces si elle n'existe pas
# Pas d'argument, pas de retour
function F_create_table_history () {

echo "INFO: Controle existence $TABLE_HISTORY et création sinon"

ret=$(F_controle_existence_table $TABLE_HISTORY)
echo "$ret"

if [ $ret -eq 0  ]; then
echo "INFO: Création $TABLE_HISTORY"
sql="CREATE TABLE $TABLE_HISTORY
(
    date_lancement date NOT NULL,
    utilisateur character varying(20) NOT NULL,
    serveur character varying(30) NOT NULL,
    grass_version character varying(10) NOT NULL,
    structure character varying(30) NOT NULL,
    scan_dir character_varying(100) NOT NULL,
    arguments text COLLATE pg_catalog."default" NOT NULL,
    parametres text COLLATE pg_catalog."default" NOT NULL,
    constantes text COLLATE pg_catalog."default" NOT NULL,
    pattern character varying(10) NOT NULL,
    schema character varying(40) NOT NULL,
    dossier character varying(100) COLLATE pg_catalog."default" NOT NULL,
    duree_script integer NOT NULL,
    nb_images_a_traiter integer NOT NULL,
    nb_images_traitees integer NOT NULL,
    statut_execution text COLLATE pg_catalog."default"
)WITH (
    OIDS = FALSE
)TABLESPACE pg_default;
GRANT ALL ON TABLE $TABLE_HISTORY TO foret_apache WITH GRANT OPTION;
GRANT ALL ON TABLE public.history_scan2surfaces TO surfaces_group;
COMMENT ON TABLE public.history_scan2surfaces
    IS 'Table pour persister la traçabilité des appels au script de calcul des surfaces.';"

  ret=$(PGPASSWORD=$PGPASSWORD psql -qtA -h $BDDHOST -U $USER_BDD -d $BDD -c "$sql")

  echo "INFO: Création de la table public.history_scan2surfaces"

  ret=$(F_controle_existence_table public.history_scan2surfaces)
  if [ $ret -eq 0 ]; then
    echo "ERREUR: problème de la création de la table public.history_scan2surfaces";
    echo "Arrêt du script"
    exit
  fi
  fi
}

# Fonction pour insérer les données de l'exécution du script dans la table d'history
# Pas de paramètres
# Pas de retour
function F_maj_history () {
  F_calcule_variables_history

  sql="INSERT INTO $TABLE_HISTORY (date_lancement,utilisateur,serveur,grass_version,arguments,parametres,constantes,pattern,schema,dossier,nb_images_a_traiter,\
nb_images_traitees,duree_script,statut_execution, structure, scan_dir) \
VALUES ('$HISTO_DATE_LANCEMENT','$HISTO_UTILISATEUR','$HISTO_SERVEUR','$HISTO_GRASS_VERSION','$HISTO_ARGUMENTS','$HISTO_PARAMETRES',\
'$HISTO_CONSTANTES','$HISTO_PATTERN','$HISTO_SCHEMA','$HISTO_DOSSIER','$HISTO_NBIMG_A_TRAITER','$HISTO_NBIMG_TRAITEES','$HISTO_DUREE',\
'$HISTO_STATUT_EXECUTION','$STRUCTURE','$SCAN_DIR')"
  
  ret=$(PGPASSWORD=$PGPASSWORD psql -qtA -h $BDDHOST -U $USER_BDD -d $BDD -c "$sql")
}

# fonction qui calcule les valeurs des variables nécessaires pour la traçabilité de l'exécution du script
function F_calcule_variables_history () {
  HISTO_DATE_LANCEMENT="$(date  +'%Y-%m-%d')"
  HISTO_UTILISATEUR="$USER"   # Utilisateur qui a lancé la session et donc le script
  HISTO_SERVEUR="$(hostname)"
  HISTO_GRASS_VERSION="$GRASS_VERSION"
  HISTO_ARGUMENTS="$ARGUMENTS"
  HISTO_PARAMETRES=$(cat $FIC_PARAM | sed -e "s/'/ /g")
  HISTO_CONSTANTES=$(cat $FIC_CONST | sed -e "s/'/ /g")
  HISTO_PATTERN=$PATTERN_SCAN
  HISTO_SCHEMA=$SCHEMA
  HISTO_DOSSIER=${BASE_DIR_SCAN}/${SCAN_DIR}
  HISTO_NBIMG_A_TRAITER=$NBIMG
  HISTO_NBIMG_TRAITEES=$imgNB
  HISTO_DUREE="$((($(date +%s) - $debut_script)))"
  HISTO_STATUT_EXECUTION=$MESSAGE_FIN_EXECUTION
}

# Fonction à appeler à la place de la commande exit pour enregistrer les valeurs de traçabilité de l'exécution avant la sortie effective du script
# @param: message de fin d'exécution (statut final)
function F_exit_history () {
  MESSAGE_FIN_EXECUTION=$1
  echo $MESSAGE_FIN_EXECUTION
  F_maj_history
  exit
}
 

# Fonction pour récupérer toutes les commandes effectuées sur une structure, depuis l'history
function F_generate_history_for_structure () {
  echo "INFO: récupération historique pour la structure $STRUCTURE"
  HISTORY_FILE=$TMPDIR/HISTORY_$SCHEMA.txt
  PGPASSWORD=foret_apache  psql -qtA -h $BDDHOST -U foret_apache -d $BDD -o $HISTORY_FILE -c "select date_lancement, utilisateur, arguments,schema,structure, scan_dir,
duree_script,nb_images_traitees from $TABLE_HISTORY where structure = '${STRUCTURE}';"
  ENTETE="date_lancement | utilisateur| arguments | schema | structure | scan_dir | duree_script | nb_images_traitees"
  sed -i "1  i\ $ENTETE" $HISTORY_FILE
}
