# Fonction de traitement pour le calcul des surfaces des graines
# Appelée par surfaces.sh, lorsque le mode graine est actif (option -ag) 

function F_algo_graines() {

       # mémorisation instant de début du script
       debut=$(date +%s)

       g.region align=scan.green
       g.remove --quiet -f type=raster name=scan.red@MAPSET,scan.blue@MAPSET

       min_growth=1000
       min_seuil=1
       previous_area=1

       for seuil in {66..94..2}
       do
         r.mapcalc expression="seuil =  if(255 * scan.green/$seuil > 255, null(), 0)" --overwrite
         r.grow --quiet --overwrite input=seuil output=seuilplus5 radius=5
         r.grow --quiet --overwrite input=seuilplus5 output=seuilmoins5 radius=-5
         surface_seuil=$(r.stats -c input=seuilmoins5 | grep '0-0 ')
         surface_seuil=${surface_seuil//0-0 /}
         echo "INFO: $seuil $surface_seuil"
         growth=$(bc <<< "10000 * ($surface_seuil - $previous_area) / $previous_area")
         echo "INFO: $growth"
         if [ $growth -lt $min_growth ]
         then
           min_growth=$growth
           min_seuil=$seuil
         fi
         previous_area=$surface_seuil
      done
      echo "INFO: selected seuil = $min_seuil"
      min_seuil_moins1=$(expr $min_seuil - 1)
      r.mapcalc expression="seuil =  if(255 * scan.green/$min_seuil > 255, null(), 0)" --overwrite
      r.grow --quiet --overwrite input=seuil output=seuilplus5 radius=$CROISSANCE
      r.grow --quiet --overwrite input=seuilplus5 output=seuilmoins5 radius=-$CROISSANCE
      r.mapcalc expression="seuilmoins5avec1 =  if(isnull(seuilmoins5),1, 0)" --overwrite
      r.contour --overwrite input=seuilmoins5avec1 output=contour_seuil levels=1 cut=$SEUIL_SURFACE
      v.type --overwrite --verbose input=contour_seuil output=boundaries to_type=boundary
      v.centroids --overwrite input=boundaries output=area
      v.extract --overwrite input=area output=area_only type=area
      v.generalize --overwrite input=area_only type=area output=area_smooth method=douglas threshold=$LISSAGE


      echo "INFO: création d'un table pour la map area_smooth"
      v.db.addtable map=area_smooth@$MAPSET layer=1 --quiet --overwrite

      echo "INFO: Ajout des attributs obligatoires (image, surf, duree, seuil_gris)"
      v.db.addcolumn --quiet map=area_smooth@$MAPSET columns="image varchar(80)"
      v.db.addcolumn --quiet map=area_smooth@$MAPSET columns="surf double precision"
      v.db.addcolumn --quiet map=area_smooth@$MAPSET columns="area_etalon integer"
      v.db.addcolumn --quiet map=area_smooth@$MAPSET columns="duree integer"
      v.db.addcolumn --quiet map=area_smooth@$MAPSET columns="seuil_gris integer"

      echo "INFO: Ajout attribut area"
      v.to.db map=area_smooth@$MAPSET option=cat columns=cat --overwrite
      v.to.db map=area_smooth@$MAPSET option=area columns=area --overwrite

#      g.copy --overwrite vect=area_smooth,$NOM_TABLE 

      F_connect_to_db_pgsql


      # calcul durée du calcul pour l'image en cours
      export DUREE=$( F_round (($(date +%s) - $debut)/60) 0 )
      ret=$(F_compare_numbers 1 $DUREE )
      if [ $ret -eq 1 ]; then
        DUREE=1
      fi
      echo "INFO: durée: $DUREE, début: $debut, fin: $(date +%s)"
      v.out.ogr --quiet -u input="area_smooth@$MAPSET" type=area 'output=PG:host=147.100.20.28 dbname=surfaces_base_uruefm user=surfacier password=surface' olayer=$SCHEMA.$NOM_TABLE layer=1 format=PostgreSQL lco=OVERWRITE=YES

      echo "INFO: Alimentation table élémentaire"
      db.execute --quiet sql="UPDATE ${SCHEMA}.$NOM_TABLE SET image = '$(basename $file)', surf = area, duree = $DUREE, area_etalon = 0, seuil_gris = $min_seuil"

      echo "INFO:  attribution des droits à foret_apache sur la table $NOM_TABLE"
      db.execute --quiet sql="GRANT ALL ON ${SCHEMA}.$NOM_TABLE TO foret_apache;"


}
