#
# Script surfaces.sh
#
# Calcul des surfaces foliaires à partir de scan
#
#  Fichier des constantes de l'application
#  Ne pas les modifier, sauf si vous savez ce que vous faites !!
#
#
# COEFF_ETALON : Valeur qui permet de passer du pixel2 au mm2
# BDD        : base de données dédiée aux surfaces
# BDDHOST    : désigne l'adresse du serveur base de données
# GREGIONS   : Défini la zone du scan à analyser pour les surfaces de végétal. [ NE PAS EFFACER: Valeurs par défaut n=6970 s=20 e=7700 w=20]
# GREGIONG   : Défini la zone de l'image exploitable pour la recherche des graines.
# ETALON_THEORIQUE: cette valeur représente la surface théorique de l'étalon. Elle a été déterminée manuellement avec les réglages "prévus" pour le scanner officiel URFM
# GREGIONE   : Défini la zone du scan à analyser pour l'étalon. 
# BASE_DIR_SCAN : racine partagée pour répertoire sur le NAS où les scans sont stockés
BDD=surfaces_base_uruefm
BDDHOST=147.100.20.28
COEFF_ETALON=556.64
GREGIONS="n=6970 s=20 e=7700 w=20"
GREGIONG="n=2850 s=125 e=2850 w=100"
#GREGIONS="n=6768 s=93 e=7614 w=93"
ETALON_THEORIQUE=149000   
GREGIONE="n=6900 s=5500 e=9700 w=8000"
BASE_DIR_SCAN="$HOME/paca-urfm/URUE/SURFACES"
# Deux algo différents: feuille ou graine. Permet de choisir la commande de mapcalc adaptée au cas d'utilisation
# valeur par défaut pour l'algo = feuille
ALGO="feuille"   
# valeur par défaut pour le pattern de recherche d'image
PATTERN_SCAN=$PATTERN_SCAN_TIFF    

# à changer dans les Scripts d'appel :
# COEFF_ETALON_SURF=556.64
# COEFF_ETALON_LIGNE=23.593

# USER       : utilisateur postgres qui créera les tables de sortie
# PGPASSWORD : mot de passe postgresde l'utilisateur
USER=surfacier
PGPASSWORD=surface
