# Fonction de traitement pour le calcul des surfaces des feuilles
# Appelée par surfaces.sh, lorsque le mode feuille est actif (par défaut)

function F_algo_feuilles() {

# mémorisation instant de début du script
debut=$(date +%s)

echo "INFO: algo qui permet de trouver les pixels de végetal"
r.mapcalc --quiet --overwrite expression=S="if((scan.green@$MAPSET + scan.red@$MAPSET) > (scan.blue@$MAPSET * ${POIDS_BLEU}),1,0) + if((scan.green@$MAPSET + scan.red@$MAPSET + scan.blue@$MAPSET<1),1,0)"


echo "INFO: entoure les pixels contigus"
r.contour --quiet --overwrite input=S@$MAPSET output=scan_contour levels=1

echo "INFO: transforme les lignes en boundaries (frontières)"
v.type --quiet --overwrite input=scan_contour@$MAPSET output=scan_bound from_type=line to_type=boundary

if [ $DISPLAY_GRASS == 1 ]; then
    echo "INFO: Affichage de scan_bound"
    d.vect --quiet map=scan_bound@$MAPSET color=yellow  fill_color=blue width=1 attribute_column=cat
fi

echo "INFO: ajout des centroides pour faire des surfaces"
v.centroids --quiet --overwrite input=scan_bound@$MAPSET output=scan_surf

if [ $DISPLAY_GRASS == 1 ]; then
    echo "INFO: Affichage de scan_surf"
    d.vect --quiet map=scan_surf@$MAPSET color=red fill_color=red width=1 attribute_column=cat
fi

echo "INFO: export vers shapefile"
v.out.ogr --quiet --overwrite input=scan_surf@$MAPSET output=${REP_OUTPUT}/scan_surf_$$.shp format=ESRI_Shapefile

echo "INFO: import du shapefile"
v.in.ogr --quiet --overwrite input=${REP_OUTPUT}/scan_surf_$$.shp output=scan_imp

if [ $DEBUG == 1 ]; then
    echo "INFO: Affichage de scan_imp"
    d.vect --quiet map=scan_imp@$MAPSET color=green  fill_color=blue width=1 attribute_column=cat
fi

echo "INFO: Nettoyage des petites surfaces (< $SEUIL_SURFACE_P2)"
v.clean --quiet input=scan_imp@$MAPSET output=scan_surf2 tool=rmarea thres=$SEUIL_SURFACE_P2 type=point,line,area --overwrite

if [ $DEBUG == 1 ]; then
    echo "INFO: Affichage de scan_surf2"
    d.vect --quiet map=scan_surf2@$MAPSET color=blue  fill_color=blue width=1 attribute_column=cat
fi

echo "INFO: Suppression des catégories d'origine"
if [ $DEBUG == 1 ]; then
  echo "v.category --overwrite input=scan_surf2@$MAPSET output=scan_cat_del option=del cat=-1"
fi
v.category --quiet --overwrite input=scan_surf2@$MAPSET output=scan_cat_del option=del cat=-1

if [ $DEBUG == 1 ]; then
    echo "INFO: Affichage de scan_cat_del"
    d.vect --quiet map=scan_cat_del@$MAPSET color=orange  fill_color=blue width=1 attribute_column=cat
fi

echo "INFO: Création d'un buffer de $BUFF pixels (en positif)"
if [ $DEBUG == 1 ]; then
  echo "v.buffer --quiet input=scan_cat_add@$MAPSET type=area output=scan_buff_plus@$MAPSET distance=$BUFF"
fi
v.buffer --quiet -t input=scan_cat_del@$MAPSET type=area output=scan_buff_plus@$MAPSET distance=$BUFF

echo "INFO: Création d'une valeur de cat constante pour pouvoir fusionner les aires au contact"
if [ $DEBUG == 1 ]; then
  echo "v.category --overwrite input=scan_buff_plus@$MAPSET output=scan_buff_cat_fix@$MAPSET option=add cat=2 step=0"
fi
v.category --quiet --overwrite input=scan_buff_plus@$MAPSET output=scan_buff_cat_fix@$MAPSET option=add cat=2 step=0

echo "INFO: Création d'un vecteur fusionnant les aires au contact"
if [ $DEBUG == 1 ]; then
  echo "v.dissolve --overwrite input=scan_buff_cat_fix@$MAPSET column=cat output=scan_fus"
fi
v.dissolve --quiet --overwrite input=scan_buff_cat_fix@$MAPSET output=scan_fus column=cat


echo "INFO: création d'un buffer de $BUFF pixels (en négatif) pour revenir aux limites originelles"
if [ $DEBUG == 1 ]; then
  echo "v.buffer --quiet input=scan_fus@$MAPSET type=area output=scan_buff_moins@$MAPSET distance=-$BUFF"
fi
v.buffer --quiet -t input=scan_fus@$MAPSET type=area output=scan_buff_moins@$MAPSET distance=-$BUFF

echo "INFO: Suppression des catégories constantes"
if [ $DEBUG == 1 ]; then
  echo "v.category --overwrite input=scan_buff_moins@$MAPSET output=scan_cat_del2@$MAPSET option=del cat=-1"
fi
v.category --quiet --overwrite input=scan_buff_moins@$MAPSET output=scan_cat_del2@$MAPSET option=del cat=-1

echo "INFO: Création des nouvelles catégories"
if [ $DEBUG == 1 ]; then
  echo "v.category --overwrite input=scan_cat_del2@$MAPSET layer=1 type=point,line,centroid,face output=scan_cat_add@$MAPSET option=add cat=1 step=1"
fi
v.category --quiet --overwrite input=scan_cat_del2@$MAPSET layer=1 type=point,line,centroid,face output=scan_cat_add@$MAPSET option=add cat=1 step=1

echo "INFO: reconstruction de la topologie"
if [ $DEBUG == 1 ]; then
  echo "v.build --overwrite --quiet map=scan_cat_add@$MAPSET"
fi
v.build --overwrite --quiet map=scan_cat_add@$MAPSET

if [ $DEBUG == 1 ]; then
    echo "INFO: Affichage du vecteur scan_cat_add"
    d.vect --quiet map=scan_cat_add@$MAPSET color=green  fill_color=blue width=1 attribute_column=cat
fi

# création d'un table pour la map scan_cat_add
v.db.addtable map=scan_cat_add@$MAPSET layer=1 --quiet

#Ajout attribut area
v.to.db --quiet map=scan_cat_add@$MAPSET option=area columns=area  --overwrite

if [ $DEBUG == 1 ]; then
    echo "INFO: Affichage du vecteur final"
    d.vect --quiet map=scan_cat_add@$MAPSET color=yellow  fill_color=blue width=1 attribute_column=cat
fi

echo "INFO: Ajout des attributs obligatoires (image, surf)"
v.db.addcolumn --quiet map=scan_cat_add@$MAPSET columns="image varchar(80)"
v.db.addcolumn --quiet map=scan_cat_add@$MAPSET columns="surf double precision"
v.db.addcolumn --quiet map=scan_cat_add@$MAPSET columns="area_etalon integer"
v.db.addcolumn --quiet map=scan_cat_add@$MAPSET columns="duree integer"

echo "INFO: export de la couche vecteur en base de données (avec la géométrie)"
if [ $DEBUG == 1 ]; then
  echo "INFO COMMANDE: v.out.ogr -u input=scan_cat_add@$MAPSET type=area output="PG:host=$BDDHOST dbname=$BDD user=$USER_BDD password=$PGPASSWORD" output_layer=${SCHEMA}.${NOM_TABLE} layer=1 format=PostgreSQL --overwrite"
fi
v.out.ogr --quiet -u input=scan_cat_add@$MAPSET type=area output="PG:host=$BDDHOST dbname=$BDD user=$USER_BDD password=$PGPASSWORD" output_layer=${SCHEMA}.${NOM_TABLE} layer=1 format=PostgreSQL --overwrite

#F_connect_to_db_pgsql


echo "INFO: conversion pixel2 vers mm2 dans la table $NOM_TABLE"
db.execute --quiet sql="UPDATE ${SCHEMA}.$NOM_TABLE SET image = '$(basename $file)', surf = round(CAST( area/$COEFF_ETALON as numeric),2)"

echo "INFO:  attribution des droits à foret_apache sur la table $NOM_TABLE"
db.execute --quiet sql="GRANT ALL ON ${SCHEMA}.$NOM_TABLE TO foret_apache;"

echo "INFO: Suppression des petites surfaces (< $SEUIL_SURFACE_MM2) de la table $NOM_TABLE"
db.execute --quiet sql="DELETE FROM  ${SCHEMA}.${NOM_TABLE} WHERE surf < $SEUIL_SURFACE_MM2"

# calcul durée du calcul pour l'image en cours
DUREE=$((($(date +%s) - $debut)/60))

# mise à jour des champs area_etalon et durée pour la table intermédiaire
PGPASSWORD=$PGPASSWORD psql -qtA -h $BDDHOST -U $USER_BDD -d $BDD -c "UPDATE $SCHEMA.$NOM_TABLE SET (area_etalon, duree) = ($AREA_ETALON, $DUREE) " 2>&1


}
