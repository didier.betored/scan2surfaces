#!/bin/bash
#
#  Script de nettoyage des données associées à un projet surface
#

# variables d'intérêt pour le fonctionnement de ces fonctions
# PARAM_DIR et FIC_PARAM
# STRUCT_DIR et STRUCTURE
# BASE_DIR_SCAN SCAN_DIR PATTERN_SCAN  

# Autres Fonctions d'intérêt
# F_controle_existence_table définie dans surfaces_tools.sh


# Fonction pour créer le nom du fichier archive
function F_generate_archive_name() {
  ARCHIVE=archive_surfaces_$(date +'%Y-%m-%d')_${STRUCTURE}.tgz
}

# Fonction pour rechercher combien il existe d'archives pour une structure
function F_nb_archives_structure(){
  F_generate_archive_name
  NB_ARCHIVES=$(ls $ARCHIVE_DIR| grep $STRUCTURE | wc -l)
  LISTE_ARCHIVES=$(ls $ARCHIVE_DIR| grep $STRUCTURE )
}


# Fonction pour détruire le schéma et toutes les tables qu'il contient
function F_destroy_schema () {
  echo "INFO: Schéma $SCHEMA supprimé"
  db.execute sql="DROP SCHEMA IF EXISTS $SCHEMA CASCADE;"
}


#  Fonction pour effacer les images Grass associées au projet et existantes dans Grass
function F_clean_raster_grass () {
  for i in $(ls $BASE_DIR_SCAN/${SCAN_DIR}/$PATTERN_SCAN)
  do
    b=$(basename $i)
    g.remove --quiet -f type=raster name=$b > /dev/null 2>&1
  done
  echo "INFO: toutes les images raster associées au projet  $STRUCTURE ont été supprimées de l'espace de travail Grass"
}

# Fonction pour supprimer tous les scans originaux (dangereux)
function F_clean_scans(){
  echo "INFO: Suppression des scans originaux dans $BASE_DIR_SCAN/${SCAN_DIR}/"
  echo ""
  F_nb_archives_structure
  if [ $NB_ARCHIVES -lt 1 ]; then
    echo "====================================================================="
    echo "RAPPEL: cette opération va supprimer tous les scans associés au projet $STRUCTURE"
    echo ""
    echo "VISIBLEMENT, il n'existe aucune archive pour ce projet $STRUCTURE"
    echo ""
    echo "WARNING: si vous n'avez pas lancé d'archivage, cette opération est vraiment non recommandée"
    echo "         TOUS LES FICHIERS SCANS ORIGINAUX SERONT DEFINITIVEMENT PERDUS !"
    echo "====================================================================="
    echo ""
  else 
    echo "====================================================================="
    echo "RAPPEL: cette opération va supprimer tous les scans associés au projet $STRUCTURE"
    echo ""
    echo "Liste des archives trouvées pour cette structure $STRUCTURE"
    echo $LISTE_ARCHIVES
    echo ""
    echo "Vous pouvez donc valider l'opération sans soucis, on pourra restaurer les données le cas échéant !"
    echo "====================================================================="
  fi
  read -r -p "Confirmez-vous la suppression des fichiers de scan (o/n) ?" rep
  if [ "$rep" != "o" ]; then
     echo "INFO: Arrêt du script demandé (pas d'effacement des scans pour la structure $STRUCTURE)"
     exit
  fi
  rm -f $BASE_DIR_SCAN/${SCAN_DIR}/$PATTERN_SCAN 
}

# Fonction pour archiver les données sources (scans) d'un projet
function F_archive_scans() {
  if [ -e $ARCHIVE_DIR/$ARCHIVE ]; then
    echo "====================================================================="
    echo "WARNING: le fichier archive $ARCHIVE_DIR/$ARCHIVE existe déjà"
    echo "====================================================================="
    read -r -p "Confirmez pour le remplacer (o/n) ?" rep
    if [ "$rep" != "o" ]; then
     echo "INFO: Arrêt du script demandé (pas d'écrasement du fichier archive existant)"
     exit
    else
     rm -r $ARCHIVE_DIR/$ARCHIVE
    fi
  fi
  tar cvf $ARCHIVE_DIR/$ARCHIVE $BASE_DIR_SCAN/${SCAN_DIR}/$PATTERN_SCAN
}

# Fonction pour dumper le schéma pg
function F_generate_dump_from_pg(){
  DUMPFILE=$TMPDIR/DUMP_$SCHEMA.sql
  echo "INFO: Génération du dump $DUMPFILE pour le schéma $SCHEMA"
  PGPASSWORD=$PGPASSWORD pg_dump -h $BDDHOST -d $BDD -U $USER_BDD -n $SCHEMA -f $DUMPFILE
}


# Fonction pour générer l'archive complète
function F_complete_archive () {
  echo "INFO: Ajout du fichier $1 dans l'archive"
  tar -rvf $ARCHIVE_DIR/$ARCHIVE $1
  rm $1
}


# Fonction pour créer le fichier Readme  pour l'archive
function F_generate_readme () {
   README_FILE=$TMPDIR/readme.txt
   cat > $README_FILE <<EOF
Cette archive contient les données associées au calcul des surfaces
Elle permet éventuellement de revenir sur d'anciennes données à des fins de traçabilité

Nom structure: $STRUCTURE
Dossier de base des scans: $SCAN_DIR

Contenu:
$DUMPFILE :  fichier dump complet du schéma $SCHEMA
$HISTORY_FILE : fichier reprenant l'ensemble des opérations effectuées sur les données correspondantes, avec ce script surfaces
$FIC_PARAM : fichier des paramètres utilisés pour cette structure
$FIC_CONST : fichier des constantes utilisées par le script surfaces.sh
$README_FILE : ce fichier 
*.tif :  l'ensemble des scans originaux 

======================================
EOF

  echo "" >> $README_FILE
  echo "Archive réalisée le $(date '+%Y-%m-%d') par l'utilisateur $USER" >> $README_FILE
}


# Fonction pour archiver toutes les données associées à un projet surface
# le dump du schéma 
# les raster sources
# les résultats en format csv générés à partir de la table de synthèse
# un readme avec la liste des commandes lancées pour ce projet de surface (récupérées depuis history) 

function F_archive_all_data(){
F_maj_history
echo "INFO: génération archive des données pour le projet $STRUCTURE"
F_generate_archive_name   # Nom du fichier archive
F_archive_scans   # création du fichier archive avec les scans
F_generate_dump_from_pg
F_generate_history_for_structure
F_generate_readme
F_complete_archive $DUMPFILE
F_complete_archive $HISTORY_FILE
F_complete_archive $README_FILE
# on met une copie des fichiers de paramétrage  et de constante dans TMPDIR pour archivage
cp $FIC_PARAM $TMPDIR/$(basename $FIC_PARAM)
cp $FIC_CONST $TMPDIR/$(basename $FIC_CONST)
F_complete_archive $TMPDIR/$(basename $FIC_PARAM)
F_complete_archive $TMPDIR/$(basename $FIC_CONST)
echo "INFO: l'archive générée est disponible dans $ARCHIVE_DIR/$ARCHIVE"
echo ""
echo "*****************************************************************"
echo "INFO: pour libérer les espaces disques, vous pouvez supprimer toutes les données avec la commande:  "
echo "./surface.sh -f $FIC_PARAM -S $STRUCTURE -D  $SCAN_DIR -clean"


}



# Fonction pour faire le ménage dans les données d'un projet
function F_clean_all_data(){
  F_generate_archive_name
  echo ""
  echo "====================================================================="
  echo "WARNING: cette opération va supprimer toutes les données associées au projet $STRUCTURE"
  echo "       * Les fichiers scan dans $BASE_DIR_SCAN/${SCAN_DIR}/"
  echo "       * les mêmes fichiers importés dans Grass pour les besoins du script"
  echo "       * Le schéma $SCHEMA et toutes les tables générées, dont la table de synthèse"
  echo ""
  echo "WARNING: si vous n'avez pas lancé d'archivage, cette opération est vraiment non recommandée"
  echo "         TOUS LES FICHIERS SCANS ORIGINAUX SERONT DEFINITIVEMENT PERDUS !"
  echo "====================================================================="
  echo ""
  read -r -p "Confirmez-vous l'opération (o/n) ?" rep
  if [ "$rep" != "o" ]; then
     echo "INFO: Arrêt du script demandé (pas d'effacement des données pour la structure $STRUCTURE)"
     exit
  fi

  echo "INFO: nettoyage de toutes les données pour le projet $STRUCTURE"
  F_clean_raster_grass
  F_clean_scans
  F_destroy_schema
  F_calcule_variables_history
  F_maj_history
  echo "INFO: Toutes les données pour le projet $STRUCTURE ont été nettoyées"
}
