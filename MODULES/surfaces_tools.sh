#!/bin/bash
#
#  Script de calcul de surface foliaires
#
#

# fonction d'affichage pour l'usage du script
function F_Usage(){
TMPVAR=$( F_read_const $FIC_CONST BASE_DIR_SCAN)
BASE_DIR_SCAN=$(eval echo $TMPVAR)
  echo "USAGE: $1 -g <nom_fichier.txt>"
  echo "USAGE: $1 -f <parametres.txt> -S <nom_structure>  -D <dir1|dir1/dir2> -algo [M|C|G]  [-fo <jpg|tif|cr2>] [-so] [-b] [-d] [-e] [-ne] [-m] [-v] [-stat]  [-U <nom_fichier_image.tif>] [ -X <image>] [-no]"
  echo " "
  echo "Script de calcul de surfaces foliaires pour un dossier de scans"
  echo "Ce script peut aussi être utilisé pour analyser des photos de graines"
  echo "Paramètres:"
  echo "    -f <parametres.txt>: (obligatoire) nom du fichier contenant les paramètres. Ce fichier est décrit ..."
  echo "    -S <nom structure>: (obligatoire) désigne le type de structuration de la table de données en sortie. Lié également à la construction des noms des fichiers scan"
  echo "                        Les structures connues: FBn, FORGENIUS, FORGENIUS_PM_methodo"
  echo "                        En cas de besoin de définir une nouvelle structure, merci de contacter un administrateur"
  echo "    -D <dir1|dir1/dir2>: (obligatoire) désigne un répertoire, ou un chemin, à partir du chemin désigné par la constante BASE_DIR_SCAN: $BASE_DIR_SCAN"
  echo "                Le nom du schéma dans la base est calculé à partir de cet argument, dans lequel on remplace les / par des _"
  echo "                Le nombre de sous-répertoires n'est pas limité. Il faut par contre respecter la consigne: aucun caractères spéciaux, accentués ou espace"
  echo ""
  echo "    -g <nom fichier de paramètres>: (optionnel) génère un fichier de paramètres avec les valeurs par défaut"
  echo ""
  echo "    -algo [M|C|G] : choix d'un algorithme de traitement. Si non précisé, l'agorithme utilisé est celui pour les feuilles vertes (algorithme historique basé sur la recherche de la couleur verte)" 
  echo "          M: (mono) algorithme par défaut si non précisé. Filtre basé sur l'exploitation canal par canal pour identifier le vert et les ombres"
  echo "          C: (composition) algorithme avec optimisation automatique d'un paramètre basé sur l'utilisation de la composition colorée. Dans ce mode, il faut impérativement que le fichier de paramètre définisse les paramètres MIN_C, MAX_C et STEP_C"
  echo "          G: (graines) algorithme avec optimisation automatique d'un paramètre, à mobiliser pour les scans de graines" 
  echo "    -fo <jpg|tif|cr2>: (optionnel) choix du format des images sources. cr2 correspondant au format raw. Par défaut, ce sont les images avec l'extension .tif qui sont recherchées".
  echo "    -b : (optionnel) à activer si les images ne contiennent aucune couleur bleue (nécroses)"
  echo "    -d : (optionnel) active le mode debug (une seule image traitée, sur une zone réduite)"
  echo "    -ne : (optionnel) désactive le controle de l'étalon (non codé) avant le calcul des surfaces"
  echo "    -e : (optionnel) force le script à n'effectuer que le controle des étalons.Rien n'est stocké en base"
  echo "    -m : (optionnel) empêche le ménage en fin de script. Activé par défaut en mode debug"
  echo "    -v : (optionnel) force l'affichage de certaines couches pendant les calculs. Forcé en mode debug"
  echo "    -U <nom_fichier_image.tif>: (optionnel) ne traitera que l'image dont le nom de fichier est donné"
  echo "    -X <image>: (optionnel) permet d'empêcher le traitement de l'image dont le nom est fourni. "
  echo "                 Cette option peut être précisée plusieurs fois pour exclure plusieurs images"
  echo "    -no: (optionnel) empêche la suppression du schéma de stockage des résultats, et ne traite que les images non intégrées dans la table de synthèse"
  echo "    ============================"
  echo "    FONCTIONS DE POST-TRAITEMENT"
  echo "    ============================"
  echo "    Les fonctionnalités suivantes sont à lancer après la génération des tables élémentaires, et surtout, après la vérification visuelle"
  echo "    des résultats obtenus. En général, cette vérification conduit malgré tout à modifier les contours des feuilles."
  echo ""
  echo "    Attention: Les fonctions suivantes ne peuvent pas être combinées avec les options -U (image unique), ou  -X (exclusion)" 
  echo ""
  echo "    -so: permet de générer la table de synthèse en base. Les surfaces des contours corrigés seront recalculées"
  echo "         Doit être lancée avec les mêmes arguments que lors de l'appel qui à généré les tables élémentaires."
  echo "    -stat: génère les calculs des statistiques sur les images. "
  echo "         Doit être lancée avec les mêmes arguments que lors de l'appel qui à généré les tables élémentaires."
  echo "    -archive: permet de générer une archive contenant toutes les données associées à un projet de calcul de surfaces" 
  echo "         Le projet est désigné par les arguments fichier de paramètre, Fichier de structure et dossier contenant les scans"
  echo "         Doit être positionné en dernier dans la liste des arguments"
  echo "    -clean: permet de supprimer l'ensemble des données associées à un projet de calcul de surfaces" 
  echo "         Le projet est désigné par les arguments fichier de paramètre, Fichier de structure et dossier contenant les scans"
  echo "         Doit être positionné en dernier dans la liste des arguments"
  echo "    -history: montre l'historique associé à un projet de calcul de surfaces"
  echo "         Le projet est désigné par les arguments fichier de paramètre, Fichier de structure et dossier contenant les scans"
  echo "         Doit être positionné en dernier dans la liste des arguments"
  echo ""
  echo "++++++++++++++++++++++"
  echo "Exploitation standard "
  echo "++++++++++++++++++++++"
  echo "$1 -g parametres_projet.txt  --> pour créer le fichier de paramètres par défaut et l'éditer pour l'adapter au cas d'usage"
  echo "$1 -f parametres_projet.txt -S FORGENIUS -D FORGENIUS/PV/Espagne/feuille  --> pour lancer l'analyse des scans et générer les tables de surfaces par scan"
  echo ""
  echo "A partir de là, il faut vérifier sous Qgis, si les contours obtenus automatiquement sont corrects, et le cas échéant les adapter au mieux"
  echo "$1 -f parametres_projet.txt -S FORGENIUS -D FORGENIUS/PV/Espagne/feuille -so   --> pour générer la table de synthèse et la vue d'exploitation finale"
  echo "$1 -f parametres_projet.txt -S FORGENIUS -D FORGENIUS/PV/Espagne/feuille -stat   --> pour générer la table des statistiques des bandes rouge, vert et bleu par contour."
  echo ""
  echo "A tout moment, on peut demander l'affichage des traitements effectués pour une structure"
  echo "En effet, dans la base de  données, on garde la trace de qui lance quoi sur quelles données ..."
  echo "$1 -f parametres_projet.txt -S FORGENIUS -D FORGENIUS/PV/Espagne/feuille -history   --> pour afficher la liste des commandes lancées sur le projet de surface désignés par les arguments."
  echo ""
  echo "# Ensuite, pour archiver, puis nettoyer les données:"
  echo "$1 -f parametres_projet.txt -S FORGENIUS -D FORGENIUS/PV/Espagne/feuille -archive   --> pour générer une archive avec toutes les données entrées et sorties"
  echo "$1 -f parametres_projet.txt -S FORGENIUS -D FORGENIUS/PV/Espagne/feuille -clean   --> pour nettoyer les données dans grass et postgre. Nécessaire pour faire de la place!"
  echo "++++++++++++++++++++++"

  exit
}

# Fonction pour convertir le nom d'un fichier de scan en nom de table et nom de scan
function F_file2vars(){
  export NOM_TABLE=t_$(echo $(basename $1) |tr '[:upper:]' '[:lower:]' | sed -e 's/.tif//' -e 's/.jpg//' -e 's/.cr2//' -e 's/-/_/g' -e 's/_$//')  # suppression extension, minuscule, conversion des - en _
  export NOM_SCAN=$(basename $1)
}

#fonction pour lecture d'un paramètre
function F_read_param(){
# 2 paramètres: le nom du fichier de paramètre et le paramètre recherché
   var=$(cat $1 |grep -v \# | grep "${2}=" | sed -e "s/${2}=//" -e 's/"//g')
   echo $var
}

#fonction pour lecture d'une constante
function F_read_const(){
# 2 paramètres: le nom du fichier des constantes et la constante recherchée
   var=$(cat $1 |grep -v \# | grep "${2}=" | sed -e "s/${2}=//" -e 's/"//g')
   echo $var
}

# fonction pour controler le mapset
function F_controle_mapset(){
mapset_lu=$(g.mapset -p --quiet)
if [ "$mapset_lu" != "$MAPSET" ]; then
   echo "==========================================================================================="
   echo "ERREUR: le mapset actif, n'est pas celui indiqué dans le fichier paramètre (MAPSET=$MAPSET)"
   echo "INFO: liste des mapsets connus dans la session en cours:"
   echo "==========================================================================================="
   g.mapset -l
   exit
fi

}

#fonction initialisation du schéma
function F_create_db_structure() {

  # controle pré-existance du schéma
  ret=$(F_controle_existence_schema)
  if [ "$ret" == "t" ]; then
     echo ""
     echo "#########################################################################################################"
     echo "#ATTENTION: le schéma $SCHEMA existe déjà dans la base de données                                       #"
     echo "#Si vous confirmez votre souhait de poursuivre, toutes les données de ce schéma vont être supprimées !  #" 
     echo "#Ceci peut être problématique si vous avez passé du temps sous Qgis pour redessiner les contours        #"
     echo "#########################################################################################################"
     echo ""
     read  -r -p "Confirmer l'effacement du schéma $SCHEMA (o/n) ?" rep
     if [ "$rep" != "o" ]; then
       echo "INFO: Arrêt du script demandée (pas d'écrasement du schéma)"
       exit
     fi
  fi   

# vérification des droits d'écriture dans la base
  ret=$( PGPASSWORD=$PGPASSWORD psql -qtA -h $BDDHOST -U $USER_BDD -d $BDD -c "CREATE SCHEMA bidon_ville AUTHORIZATION $USER_BDD" 2>&1)
  if [ $DEBUG == 1 ]; then
    echo "INFO: retour test droits sur schéma $SCHEMA pour utilisateur $USER_BDD: $ret"
  fi
  var=$(echo $ret | grep "denied" | wc -l)
  if [ $var -gt 0  ]; then
     echo "==========================================================================================="
     echo "ERREUR: Il semble que l'utilisateur $USER_BDD ne dispose pas des droits suffisants pour créer le schéma $SCHEMA dans la base $BDD"
     echo "        Contactez un administrateur ..."
     echo "==========================================================================================="
   exit
  else
     ret=$( PGPASSWORD=$PGPASSWORD psql -qtA -h $BDDHOST -U $USER_BDD -d $BDD -c "DROP SCHEMA bidon_ville" 2>&1)
  fi

# creation du schéma: s'il existe, il est d'abord effacé
  db.execute sql="DROP SCHEMA IF EXISTS $SCHEMA CASCADE; CREATE SCHEMA $SCHEMA AUTHORIZATION $USER_BDD;"
  db.execute sql="COMMENT ON SCHEMA $SCHEMA IS 'Surfaces pour le projet : $SCHEMA';"
  db.execute sql="GRANT ALL ON SCHEMA $SCHEMA TO foret_apache;"   
  echo "INFO: Création du schéma $SCHEMA et attribution des droits à l'utilisateur foret_apache"
}

#Fonction pour le controle des arguments
function F_read_args() {
NOBLUE=0  # flag activé pour signifier que les scans ne contiennent pas de bleu
DEBUG=0   # flag pour activer le mode debug
ETALON=1  # flag pour désactiver le calcul de la surface de l'étalon
ONLY_ETALON=0 # flag pour ne traiter que le controle de la surface de l'étalon
MENAGE=1  # flag pour empécher le script de faire le ménage à la fin (par défaut, le ménage est fait à la fin de chaque image traitée)
DISPLAY_GRASS=0 # flag pour activer l'affichage de certaines couches pendant les calculs
NO_OVERWRITE=0 # flag pour empêcher la suppression du schéma s'il existe, et ne pas recalculer les images déjà traitées
STAT=0
SYNTHESE_ONLY=0 # flag pour indiquer de ne faire que la génération de la table de synthèse
ARCHIVAGE=0 # flag pour demander la création d'une archive des données pour la structure
NETTOYAGE=0  # flag pour demander le nettoyage des fichiers associés à une structure (bdd, scan, scan dans grass)
HISTORIQUE=0  # flag pour demander l'affichage des commandes passées pour une structure
PATTERN_SCAN="*.tif"   # valeur par défaut pour 
i=0

if [ $# -lt 1 ]; then
  F_Usage $0
fi

nargs=$#
while [ $i -lt $nargs ]
do
  case "$1" in 
     "-f")
       shift
       # vérification présence du fichier des paramètres
       if [ ! -f "$PARAM_DIR/$1" ]; then
          echo "==========================================================================================="
          echo "ERREUR: le fichier de paramètres ($1 ) est inaccessible dans $PARAM_DIR"
          echo "==========================================================================================="
          F_Usage $0
       fi
       FIC_PARAM=$PARAM_DIR/$1
       echo "INFO: Fichier de paramètre = $FIC_PARAM"
       i=$((i + 1))
       ;;
    "-fo")
        shift
        #echo " 25 dollar $1"
	case "$1" in
          "jpg")
              PATTERN_SCAN="*.jpg"
              echo "INFO: Format des images = jpeg"
              ;;
	  "tif")
              PATTERN_SCAN="*.tif"
              echo "INFO: Format des images = tif"
              ;;
         "cr2")
              PATTERN_SCAN="*.CR2"
              echo "INFO: Format des images = raw"
              ;;
         *)
              echo "==========================================================================================="
	      echo "ERREUR: format d'image $1 inconnu"
              echo "==========================================================================================="
              F_Usage $0
              ;;
        esac
        i=$((i + 1))  
	;;
    "-algo")
        shift
        case "$1" in 
          "M")
	     source $MODULES_DIR/surfaces_feuilles_vertes.sh
             ALGO="feuilles_vertes"
             echo "INFO: Algorithme feuille monochrome"
   	     ;;
	  "C")
             source $MODULES_DIR/surfaces_feuilles.sh
             ALGO="feuilles"
	     echo "INFO: Algorithme feuille composition colorée"
	     ;;
	  "G")
             source $MODULES_DIR/surfaces_graines.sh
             ALGO="graines"
             echo "INFO: Algorithme graine"
             ETALON=0
             echo "INFO: désactivation automatique du controle de l'étalon"
            ;;
          *)
              echo "==========================================================================================="
              echo "ERREUR: identifiant d'algorithme $1 inconnu"
              echo "==========================================================================================="
              F_Usage $0
              ;;
        esac
        i=$((i + 1))
        ;;
    "-so")
       SYNTHESE_ONLY=1
       NO_OVERWRITE=1
       echo "INFO: Activation régénération de la synthèse"
       ;; 
    "-g")
       shift
       if [ $# -ne 1 ]; then
          echo "==========================================================================================="
          echo "ERREUR: le paramètre -g doit être suivi d'un nom de fichier"
          echo "==========================================================================================="
          F_Usage $0
       fi
       F_generate_parametre $PARAM_DIR/$1
       ;;
    "-D")
       shift
       SCAN_DIR=$1
       echo "INFO: chemin vers les scans = $SCAN_DIR"
       i=$((i + 1))
       ;;
    "-b")
       NOBLUE=1
       echo "INFO: activation mode sans bleu"
       ;;
    "-d")
       DEBUG=1
       DISPLAY_GRASS=1
       MENAGE=0
       echo "INFO: activation du mode debug --> une seule image traitée, sur zone restreinte si définie; pas de ménage en fin de script"
       ;;
    "-e")
       ONLY_ETALON=1
       echo "INFO: le script ne vérifie que la conformité des étalons"
       ;;
    "-ne")
       ETALON=0
       echo "INFO: désactivation du controle de l'étalon"
       ;;
   "-m")
       MENAGE=0
       echo "INFO: le script ne fera pas le ménage à la fin de l'exécution"
       ;;
   "-v")
       DISPLAY_GRASS=1
       echo "INFO: activation de l'affichage de certaines couches pendant les calculs"
       ;;
   "-stat")
       STAT=1
       NO_OVERWRITE=1   #on empêche l'effacement du schéma en début de script dans ce cas
       echo "INFO: activation du calcul des statistiques pour les couches raster finales"
       ;;
  "-S")
      shift
      STRUCTURE=$1
      echo "INFO: Identification structure retenue pour les données de sorties (postgres): $STRUCTURE"
      i=$((i + 1))  
      ;;
  "-U")
      shift
      IMAGE_UNIQUE=$1
      echo "INFO: activation mode image unique ($IMAGE_UNIQUE)"
      i=$((i + 1))  
      ;;
  "-X")
      shift
      if [ -z "$EXCLUDE_LIST" ]; then
        EXCLUDE_LIST=$1
      else
        EXCLUDE_LIST+=$1
      fi
      echo "INFO: Activation mode exclusion d'image ($1)"
      i=$((i + 1))  
      ;;
  "-no")
      NO_OVERWRITE=1
      echo "INFO: Activation du mode no overwrite, qui empêche d'écraser les calculs déjà effectués"
      ;;
  "-archive")
      if [ -z "$SCAN_DIR" ]; then
        echo "==========================================================================================="
	echo "ERREUR: impossible de lancer un archivage sans connaître le dossier où sont stockés les scans. Assurez vous de placer cet argument à la fin de la ligne de commande"
        echo "==========================================================================================="
        F_Usage $0
      fi
      if [ -z "$STRUCTURE" ]; then
        echo "==========================================================================================="
	echo "ERREUR: impossible de lancer un archivage sans connaître le fichier de la structure. Assurez vous de placer cet argument à la fin de la ligne de commande"
        echo "==========================================================================================="
        F_Usage $0
      fi
      if [ -z "$FIC_PARAM" ]; then
        echo "==========================================================================================="
	echo "ERREUR: impossible de lancer un archivage sans connaître le fichier de paramètres. Assurez vous de placer cet argument à la fin de la ligne de commande"
        echo "==========================================================================================="
        F_Usage $0
      fi
      ARCHIVAGE=1
      ;;
  "-clean")
      if [ -z "$SCAN_DIR" ]; then
        echo "==========================================================================================="
	echo "ERREUR: impossible de lancer un nettoyage sans connaître le dossier où sont stockés les scans. Assurez vous de placer cet argument à la fin de la ligne de commande"
        echo "==========================================================================================="
        F_Usage $0
      fi
      if [ -z "$STRUCTURE" ]; then
        echo "==========================================================================================="
	echo "ERREUR: impossible de lancer un nettoyage sans connaître le fichier de la structure. Assurez vous de placer cet argument à la fin de la ligne de commande"
        echo "==========================================================================================="
        F_Usage $0
      fi
      if [ -z "$FIC_PARAM" ]; then
        echo "==========================================================================================="
	echo "ERREUR: impossible de lancer un nettoyage sans connaître le fichier de paramètres. Assurez vous de placer cet argument à la fin de la ligne de commande"
        echo "==========================================================================================="
        F_Usage $0
      fi
      NETTOYAGE=1
      ;;
  "-history")
      if [ -z "$SCAN_DIR" ]; then
        echo "==========================================================================================="
	echo "ERREUR: impossible de lancer un historique sans connaître le dossier où sont stockés les scans. Assurez vous de placer cet argument à la fin de la ligne de commande"
        echo "==========================================================================================="
        F_Usage $0
      fi
      if [ -z "$STRUCTURE" ]; then
        echo "==========================================================================================="
	echo "ERREUR: impossible de lancer un historique sans connaître le fichier de la structure. Assurez vous de placer cet argument à la fin de la ligne de commande"
        echo "==========================================================================================="
        F_Usage $0
      fi
      if [ -z "$FIC_PARAM" ]; then
        echo "==========================================================================================="
	echo "ERREUR: impossible de lancer un historique sans connaître le fichier de paramètres. Assurez vous de placer cet argument à la fin de la ligne de commande"
        echo "==========================================================================================="
        F_Usage $0
      fi
      HISTORIQUE=1
      ;;
   *)
      echo "==========================================================================================="
      echo "ERREUR: argument $1 invalide; $*"
      echo "==========================================================================================="
      F_Usage $0
      ;;
  esac
  shift
  i=$((i + 1))
done

if [ -z "$ALGO" ]; then
   echo "INFO: pas d'option algo. Algorithme 'M' sélectionné par défaut" 
   ALGO="feuilles_vertes"  # algorithme par défaut = analyse de scan pour le calcul des surfaces foliaires
   source $MODULES_DIR/surfaces_feuilles_vertes.sh
fi

if [ -z "$FIC_PARAM" ]; then  
  echo "==========================================================================================="
  echo "ERREUR: le fichier de paramètre est obligatoire"
  echo "==========================================================================================="
  F_Usage $0
fi

if [ -z "$STRUCTURE" ]; then
  echo "==========================================================================================="
  echo "ERREUR: Il faut choisir impérativement une structure des restitutions des résultats"
  echo "==========================================================================================="
  F_Usage $0
fi
if [ -z "$SCAN_DIR" ]; then
  echo "==========================================================================================="
  echo "ERREUR: le paramètre -D est obligatoire"
  echo "==========================================================================================="
  F_Usage $0
fi

if [[ $STAT -eq 1 ]] && [[ ( ! -z $EXCLUDE_LIST ) || ( ! -z $IMAGE_UNIQUE ) ]]; then
  echo "==========================================================================================="
  echo "ERREUR: le mode stat ne peut pas être activé si les options -U ou -X sont utilisées"
  echo "==========================================================================================="
  F_Usage $0
fi

}

# Fonction qui attend qu'une touche soit pressée
# ne peut agir qu'en mode DEBUG
function F_pause() {
#  if [ $DEBUG == 1 ]; then
#    read -p "Pressez ENTER pour continuer"
#  fi
  echo -n ""
}


# fonction pour mettre en place les fonctions spécifiques aux structures retenues
function F_Structure () {

case $STRUCTURE in
  "FBn")
     source $STRUCT_DIR/surfaces_structure_FBn.sh
     ;;
  "FBnArnaud")
     source $STRUCT_DIR/surfaces_structure_FBnArnaud.sh
     ;;
  "FORGENIUS")
     source $STRUCT_DIR/surfaces_structure_FORGENIUS.sh
     ;;
  "FORGENIUS_PM_methodo")
     source $STRUCT_DIR/surfaces_structure_FORGENIUS_PM_methodo.sh
     ;;
  "FORGENIUS_PM_Gmin")
     source $STRUCT_DIR/surfaces_structure_FORGENIUS_PM_Gmin.sh
     ;;
   "FORGENIUS_hetre")
     source $STRUCT_DIR/surfaces_structure_FORGENIUS_hetre.sh
     ;;
   "FORGENIUS_map_hetre")
     source $STRUCT_DIR/surfaces_structure_FORGENIUS_map_hetre.sh
     ;;
  "cormier")
     source $STRUCT_DIR/surfaces_structure_cormier.sh
     ;;
  "Alice")
     source $STRUCT_DIR/surfaces_structure_Alice.sh
     ;;
    "cypres_cp")
     source $STRUCT_DIR/surfaces_structure_cypres_cp.sh
     ;;
    "RH")
     source $STRUCT_DIR/surfaces_structure_RH.sh
     ;;
   "GRAINES")
     source $STRUCT_DIR/surfaces_structure_graines.sh
     ;;

  *) 
    echo "==========================================================================================="
    echo "ERREUR: Nom de structure de sortie inconnu. Arrêt du script"
    echo "Editez le fichier surfaces_tools.sh pour mettre à jour les structures connues, ou corrigez l'appel au script"
    echo "==========================================================================================="
    exit
    ;;
esac
}
 
# fonction pour mettre en place la connexion à la base postgresql
function F_connect_to_db_pgsql(){
  echo "INFO: Connexion à la base : base=$BDD,  serveur=$BDDHOST, user=$USER_BDD, schéma: $SCHEMA"
  db.connect --quiet driver=pg database="$BDD" schema="$SCHEMA"
  db.login --overwrite driver=pg user=$USER_BDD password=$PGPASSWORD host=$BDDHOST port=5432
}


# Fonction pour tester l'existence d'une table dans la base
# @param: si un paramètre est utilisé, il s'agit de la référence à une table particulière ($1)
#          sinon, on vérifie la présence de la table $SCHEMA.$NOM_TABLE
# valeur de retour: true or false suivant que la table interrogée  existe ou pas
function F_controle_existence_table(){

if [ $# -lt 1 ]; then
  table=$SCHEMA.$NOM_TABLE
else
  table=$1
fi
  ret=$(db.tables -p driver=pg database=$BDD | grep  $table | wc -l)
  echo $ret
}

# Fonction pour tester l'existence du schéma 
function F_controle_existence_schema() {
  ret=$(PGPASSWORD=$PGPASSWORD psql -qtA -h $BDDHOST -U $USER_BDD -d $BDD -c "SELECT EXISTS(SELECT 1 FROM pg_namespace WHERE nspname = '$SCHEMA'); ")
  if [ "$DEBUG" == "1" ]; then
    echo "INFO: retour test existance schéma: $ret"
  fi
  echo $ret
}

# Fonction pour générer le fichier de paramètre par défaut
function F_generate_parametre(){
if [ -f $1 ]; then
   echo "ATTENTION: le fichier $1 existe déjà."
   read -r -p "Souhaitez vous poursuivre (o/n)?" rep
   if [ "$rep" != "o" ]; then
     echo "INFO: Arrêt du script demandé (pas d'écrasement du fichier de paramètre existant)"
     exit
   fi
fi
cp PARAMETRAGE/param_generique.txt $1
echo "INFO: fichier de paramètre $1 généré"
exit

}


function F_controle_scan_dispo () {
# controle existance du dossier de base des scans
if [ ! -d  "$BASE_DIR_SCAN" ];   then
    echo "==========================================================================================="
    echo "ERREUR: Répertoire de base $BASE_DIR_SCAN non trouvé (controlez le fichier de paramètre)"
    echo "==========================================================================================="
    exit
fi

# controle existance des sous-dossiers de scans
if [ ! -d "${BASE_DIR_SCAN}/$SCAN_DIR" ]; then
   echo "==========================================================================================="
   echo "ERREUR: Répertoire des scans $SCAN_DIR dans  $BASE_DIR_SCAN non trouvé (controlez l'argument fourni à l'option -D au lancement du script)"
   echo "==========================================================================================="
   exit
fi

# controle présence de fichiers  de scans
NBIMG=$(ls ${BASE_DIR_SCAN}/${SCAN_DIR}/$PATTERN_SCAN  | wc -l)
if [ $NBIMG -lt 1 ]; then
   echo "==========================================================================================="
   echo "ERREUR: pas de fichiers $PATTERN_SCAN trouvés dans ${BASE_DIR_SCAN}/${SCAN_DIR}"
   echo "==========================================================================================="
   exit
fi
if [ ! -z "$IMAGE_UNIQUE" ]; then
  echo "INFO: Nombre d'images à traiter: 1 ($NBIMG images trouvées)"
else 
  echo "INFO: Nombre d'images à traiter: $NBIMG"
fi
}

# fonction pour effacer la table synthèse si elle existe déjà
# Pour la régénérer ensuite
function F_delete_synthese() {
 db.execute --quiet "DROP TABLE IF EXISTS $SCHEMA.$SCHEMA CASCADE"
}



# Fonction pour calculer la balance des blancs sur les images (canal rouge) lorsqu'on est en mode graines
# Utilisé pour étaler la dynamique 
function F_balance_des_blancs() {
  echo "INFO: Appel fonction balance des blancs"
  g.region res=1 w=1000 e=2000 s=1000 n=2000 
  r.mapcalc expression="scan_blanc=scan.red" --overwrite 
  colMax=$(r.info -r scan_blanc | grep max=) 
  colorMax=${colMax//max=/} 
  g.region rast=scan.red 
  r.mapcalc "scan_rescaled =  if(255 * scan.red/$colorMax > 255, 255, 255 * scan.red/$colorMax)" --overwrite 
  r.colors map=scan_rescaled color=grey
}

# fonction pour vérifier si le premier argument est supérieur ou égal au second
# ces nombres sont supposés être des flottants
function F_compare_numbers () {
  if awk "BEGIN {exit !($1 >= $2)}"; then
     echo 1
  else
     echo 0
  fi
}

#Fonction pour calculer un arrondi avec bc
# deux arguments: la valeur à traiter et la précision pour le nombre de décimales en sortie
F_round()
{
echo $(printf %.$2f $(echo "scale=$2;(((10^$2)*$1)+0.5)/(10^$2)" | bc))
};
