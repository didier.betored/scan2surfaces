
# script appelé par surfaces.sh par défaut. Sauf si l'argument "-n"  a été utilisé au lancement de surfaces.sh
#
# La surface de l'étalon est stockée dans la variable AREA_ETALON, pour comparaison par le script appelant (surfaces.sh)
#

g.region  --quiet --overwrite $GREGIONE save=etalon     # Region pour l'étalon
echo "INFO: Région utilisée pour l'étalon = $GREGIONE"
g.region --quiet region=etalon@$MAPSET

# suppression des images si elles existent
g.remove --quiet -fb type=all pattern="etalon*"

echo "INFO: file = $file"
r.in.gdal --quiet input=$file output='etalon' -o --overwrite 

#d.rast --quiet etalon.blue@$MAPSET  

r.composite --quiet --overwrite red=etalon.red@$MAPSET green=etalon.green@$MAPSET blue=etalon.blue@$MAPSET output=etalon 

if [ $DEBUG == 1 ]; then
  d.rast --quiet etalon@$MAPSET 
  F_pause
fi

r.contour --quiet --overwrite input=etalon@$MAPSET output=etalon_contour levels=10000

if [ $DEBUG == 1 ]; then
  d.vect --quiet map=etalon_contour color='red' 
  F_pause
fi

v.type --overwrite input=etalon_contour@$MAPSET output=etalon_bound from_type=line to_type=boundary --quiet

v.centroids --overwrite input=etalon_bound@$MAPSET output=etalon_surf  --quiet

v.out.ogr --overwrite input=etalon_surf@$MAPSET output=$REP_OUTPUT/etalon_surf_$$.shp format=ESRI_Shapefile --quiet
v.in.ogr input=$REP_OUTPUT/etalon_surf_$$.shp output=etalon_imp --quiet
v.clean input=etalon_imp@$MAPSET output=etalon_surf tool=rmarea thres=300 type=point,line,area --overwrite --quiet

v.generalize --overwrite input=etalon_surf@$MAPSET output=etalon_liss method=chaiken threshold=0.5 --quiet

v.category --overwrite input=etalon_liss@$MAPSET output=etalon_cat_del option=del cat=-1  --quiet
#v.category input=etalon_liss@$MAPSET option=report
v.category --overwrite input=etalon_cat_del@$MAPSET layer=1 type=point,line,centroid,face output=etalon_cat_add option=add cat=1 step=1  --quiet
#v.category input=etalon_cat_add@$MAPSET option=report
if [ $DEBUG == 1 ]; then
   d.vect --overwrite map=etalon_cat_add@$MAPSET color=yellow  fill_color=blue width=1 attribute_column=cat  --quiet
fi

v.db.addcolumn --overwrite map=etalon_cat_add columns="area DOUBLE PRECISION"  --quiet

v.to.db --overwrite map=etalon_cat_add option=area columns=area  --quiet
#v.db.select map=etalon_cat_add
STRING_ETALON=$(v.db.select map=etalon_cat_add )
AREA_ETALON=$(v.db.select map=etalon_cat_add |sed -e 's/|/;/g' | grep "1;" | awk -F";" '{print $4}'  )

if [ $MENAGE == 1 ]; then
   g.remove --quiet -fb type=all pattern="etalon*"
fi






















