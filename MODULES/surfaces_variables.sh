# Dossier pour les différents modules du script
export MODULES_DIR=./MODULES
# Dossier pour les fichiers de paramètres
export PARAM_DIR=./PARAMETRAGE
# Dossier pour les fichiers associés aux structures
export STRUCT_DIR=./STRUCTURE
# Dossier pour stocker les archives
export ARCHIVE_DIR=./ARCHIVES
# Dossier pour stocker les fichiers temporaires lorsque nécessaire
export TMPDIR=./TMP
