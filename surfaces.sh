#!/bin/bash
#
#  Script de calcul de surfaces foliaires à partir de fichiers images issus de scan
#  Ce script se lance depuis une session grass démarrée
#  Les résultats produits sont intégrés dans une table d'une base de données postgre 
#  Voir le fichier Readme.md pour plus de détails
#
# Auteurs : Philippe Clastre/Didier  Betored
# Version 2.0
#
#

# Chargement des fonctions utilitaires
source ./MODULES/surfaces_variables.sh
source $MODULES_DIR/surfaces_tools.sh
source $MODULES_DIR/surfaces_history.sh
source $MODULES_DIR/surfaces_archive_clean.sh
FIC_CONST=$MODULES_DIR/surfaces_constantes.txt   # cette variable désigne le fichier contenant les constantes

ARGUMENTS="$@"

# Enregistrement du démarrage 
debut_script=$(date +%s)

# controle grass en cours d'exécution et version
if [ -z "$GRASS_VERSION" ]; then
  echo "ERREUR: le script $0 doit se lancer dans une session grass"
elif [ "$GRASS_VERSION" != "7.8.5" ]; then
     echo "WARNING: ce script a été testé sur une version Grass 7.8.5."
     echo "WARNING: la version en cours d'exécution est $GRASS_VERSION"
     read -p "Tapez ENTER pour poursuivre ou Ctrl-C pour arrêter le script"
fi

# Lecture des paramètres d'appel du script
F_read_args $@

# Chargement des fonctions de structure (pilotage des variables en sortie dans la table de synthèse)
# Le choix du fichier de fonctions est piloté par un argument à l'appel du script
F_Structure

# récupération des constantes
BDD=$(F_read_const $FIC_CONST BDD)
BDDHOST=$(F_read_const $FIC_CONST BDDHOST)
GREGIONS=$(F_read_const $FIC_CONST GREGIONS)
COEFF_ETALON=$(F_read_const $FIC_CONST COEFF_ETALON)
ETALON_THEORIQUE=$(F_read_const $FIC_CONST ETALON_THEORIQUE)
GREGIONE=$(F_read_const $FIC_CONST GREGIONE)
TMPVAR=$( F_read_const $FIC_CONST BASE_DIR_SCAN)
BASE_DIR_SCAN=$(eval echo $TMPVAR)
USER_BDD=$(F_read_const $FIC_CONST USER)
PGPASSWORD=$(F_read_const $FIC_CONST PGPASSWORD)

# Récupération des paramètres qui pilotent le fonctionnement du script
MAPSET=$(F_read_param $FIC_PARAM MAPSET)
REP_OUTPUT=/tmp   # pour les fichiers intermédiaires
SEUIL_SURFACE_P2=$(F_read_param $FIC_PARAM SEUIL_SURFACE_P2)
SEUIL_SURFACE_MM2=$(F_read_param $FIC_PARAM SEUIL_SURFACE_MM2)
DEBUGGREGION=$(F_read_param $FIC_PARAM DEBUGGREGION)
POIDS_BLEU=$(F_read_param $FIC_PARAM POIDS_BLEU)
EROSION=$(F_read_param $FIC_PARAM EROSION)
BUFF=$(F_read_param $FIC_PARAM BUFF)
MIN_C=$(F_read_param $FIC_PARAM MIN_C)
MAX_C=$(F_read_param $FIC_PARAM MAX_C)
STEP_C=$(F_read_param $FIC_PARAM STEP_C)

# cas pour l'algo C
if  [[ "$ALGO" == "feuilles"  && ( -z "$MIN_C" || -z "$MAX_C" || -z "$STEP_C" ) ]]; then
  echo "==========================================================================================="
  echo "ERREUR: tu as choisi l'algorithme 'C' mais au moins un des paramètres attendus n'a pas été trouvé dans le fichier de paramètre" 
  echo "       Vérifie que les paramètres MIN_C, STEP_C ET MAX_C sont bien présents et contiennent des valeurs pertinentes "
  echo ""
  echo "Impossible de poursuivre. Arrêt du script"
  echo "==========================================================================================="
  exit -1
fi

# Cas pour les graines
if [ "$ALGO" == "graines" ]; then
   GREGIONS=$(F_read_const $FIC_CONST GREGIONG)
   CROISSANCE=$(F_read_param $FIC_PARAM CROISSANCE)
   SEUIL_SURFACE=$(F_read_param $FIC_PARAM SEUIL_SURFACE)
   LISSAGE=$(F_read_param $FIC_PARAM LISSAGE)
fi
exit
# Autres variables
SCHEMA=$(echo ${SCAN_DIR}| sed -e 's,/,_,g' -e 's/_$//'|tr '[:upper:]' '[:lower:]' )
echo "INFO: Nom du schéma en base: $SCHEMA"

# Si archivage demandé, on ne fait que ça 
if [ $ARCHIVAGE == 1 ]; then
  NBIMG=0
  imgNB=0
  F_archive_all_data
  exit
fi

# Si nettoyage demandé, on ne fait que ça
if [ $NETTOYAGE == 1 ]; then
  NBIMG=0
  imgNB=0
  F_clean_all_data
  exit
fi

# Si historique demandé, on ne fait que ça 
if [ $HISTORIQUE == 1 ]; then
  NBIMG=0
  imgNB=0
  F_generate_history_for_structure
  echo "INFO: affichage des sorties de l'historique pour la structure $STRUCTURE"
  echo "================================================================================"
  cat $HISTORY_FILE
  echo "================================================================================"
  exit
fi

# Controles sur dossier des scans
F_controle_scan_dispo

# Controle du mapset sélectionné
F_controle_mapset

# connexion à la bdd postgres pour créer le schéma et la table de synthèse
F_connect_to_db_pgsql

# en mode NO_OVERWRITE, on n'efface pas le schéma et on ne créé pas la table de synthèse qui est réputée exister
# par un lancement précédent
if [ $NO_OVERWRITE == 0 ]; then
  echo "INFO: Ménage en base et création du schéma $SCHEMA"
  F_create_db_structure
  #echo "INFO: création table de synthèse"
  #F_create_table_synthese
fi

# si la table d'history n'existe pas elle est créé (source dans surfaces_history.sh)
F_create_table_history

echo "INFO: suppression des couches temporaires existantes"
g.remove --quiet -f type=all pattern="scan*, *.tif, *.cr2, *.jpg"

g.mapsets --quiet mapset=$MAPSET

imgNB=1


####################################################################################################
#  Mode synthèse seulement: après une exécution normale, on peut éventuellement intervenir avec
#  Qgis pour corriger certains vecteurs. Il faut alors demander la régénération de la table de synthèse
#   (argument -so)
####################################################################################################
if [ $SYNTHESE_ONLY == 1 ]; then
  # controle pré-existance du schéma
  ret=$(F_controle_existence_schema)
  if [ "$ret" == "f" ]; then
    echo "ERREUR: le schéma n'existe pas. Pour utiliser le mode 'so', il faut que le script ait tourné normalement auparavant"
    exit
  fi

  PGPASSWORD=$PGPASSWORD psql -qtA -h $BDDHOST -U $USER_BDD -d $BDD -c "DROP TABLE IF EXISTS $SCHEMA.$SCHEMA CASCADE" 2>&1

  # Effacement de la table synthèse si elle existe déjà
  F_delete_synthese

  # création de la table de synthèse
  F_create_table_synthese
  
  for file in ${BASE_DIR_SCAN}/${SCAN_DIR}/$PATTERN_SCAN
  do
    NOM_TABLE=t_$(echo $(basename $file) |tr '[:upper:]' '[:lower:]' | sed -e 's/.tif//' -e 's/.cr2//' -e 's/.jpg//' -e 's/-/_/g' -e 's/_$//')  
    # suppression extension, minuscule, conversion de>
    
    # controle pré-existance de la table
    ret=$(F_controle_existence_table)
    if [ "$ret" == "f" ]; then
      echo "WARNING: la table $TABLE n'existe pas dans le schéma $SCHEMA."
      read -r -p "Pressez une touche pour poursuivre ..." 
    else
      F_alimente_table_synthese
    fi
  done

  # Appel création vue  de synthèse (définie dans le fichier structure)
  F_create_view

  F_exit_history "INFO: table de synthèse $SCHEMA.$SCHEMA et vue  $SCHEMA.${SCHEMA}_view générées"
fi

###################################################################################################
# Boucle de traitement pour chaque scan, utilisé en mode ajout des statistiques pour les couleurs
#       Quand on entre dans la boucle, on est connecté sur la bdd
##################################################################################################

if  [[ $STAT -eq 1 ]] ; then 

  echo "INFO: Mode calcul des statistiques"
  # connexion à la bdd postgres
  #F_connect_to_db_pgsql

  for file in ${BASE_DIR_SCAN}/${SCAN_DIR}/$PATTERN_SCAN
  do
    NOM_TABLE_SOURCE=t_$(echo $(basename $file) |tr '[:upper:]' '[:lower:]' | sed -e 's/.tif//' -e 's/.cr2//' -e 's/.jpg//' -e 's/-/_/g' -e 's/_$//')  # suppression extension, minuscule, conversion des - en _
    NOM_SCAN=$(basename $file)

    # On définit la région d'étude
    echo "INFO: définition de la zone de travail sur le scan (paramétrable)"
    if [ $DEBUG == 1 ]; then
      g.region  --overwrite $DEBUGGREGION save=region --quiet   # Region restreinte en mode debug, si définie dans le fichier paramètre
      echo "INFO: Région utilisée $DEBUGGREGION"
    else
       g.region  --overwrite $GREGIONS save=region  --quiet   # Région normale
       echo "INFO: Région utilisée $GREGIONS"
    fi

    # on applique la région d'étude
    g.region region=region@$MAPSET --quiet

    echo "INFO: Import du scan dans GRASS"
    r.in.gdal input=$file output=scan --quiet -o --overwrite

    # On ne sait pas dire à v.rast.stats d'écrire directement dans la table destination: on passe par une table intermédiaire RVB
    echo "INFO: Création couche vecteur RVB (v.in.ogr) exploité par v.rast.stats"
    v.in.ogr  input="PG:host=$BDDHOST dbname=$BDD user=$USER_BDD password=$PGPASSWORD" output=RVB layer=${SCHEMA}.${NOM_TABLE_SOURCE}  --overwrite

    # calcul des statistiques sur les images
    v.rast.stats --quiet -c map=RVB@$MAPSET raster=scan.blue@$MAPSET  column_prefix=blue  method=average
    v.rast.stats --quiet -c map=RVB@$MAPSET raster=scan.red@$MAPSET   column_prefix=red   method=average
    v.rast.stats --quiet -c map=RVB@$MAPSET raster=scan.green@$MAPSET column_prefix=green method=average

    # copie des valeurs dans la table définitive et suppression des données temporaire
    PGPASSWORD=$PGPASSWORD psql -qtA -h $BDDHOST -U $USER_BDD -d $BDD -c "update  ${SCHEMA}.${NOM_TABLE_SOURCE} AS d set rouge = s.red_average, vert= s.green_average, bleu=s.blue_average from ${SCHEMA}.RVB s where s.cat = d.cat; drop table ${SCHEMA}.RVB;"
  done
  exit   
fi

###################################################################################################
# Boucle de traitement pour chaque scan
#       Quand on entre dans la boucle, on est connecté sur la bdd
##################################################################################################
echo "INFO: début boucle image dans ${BASE_DIR_SCAN}/${SCAN_DIR}/$PATTERN_SCAN"

for file in ${BASE_DIR_SCAN}/${SCAN_DIR}/$PATTERN_SCAN
do
F_file2vars  $file   # génère les variables NOM_TABLE et NOM_SCAN utilisées par la suite
#echo "INFO: fichier lu: $file"
#echo "INFO: table bdd: $NOM_TABLE"
#echo "INFO: nom scan: $NOM_SCAN"

# connexion à la bdd postgres pour créer le schéma et la table de synthèse
#F_connect_to_db_pgsql

# controle de l'existence de la table de sortie temporaire $TABLE
if [ $NO_OVERWRITE == 1 ]; then
  status=$(F_controle_existence_table)
  if [ "$status" == "1"  ]; then
	echo "INFO: la table $NOM_TABLE existe déjà. Passage à l'image suivante"
	continue
  else
      echo "INFO: la table $NOM_TABLE n'existe pas. Lancement calcul"
  fi
else
  db.execute sql="DROP TABLE IF EXISTS $SCHEMA.$NOM_TABLE"   > /dev/null 2>&1
  # création des séquences dans le nouveau schéma
  db.execute sql="CREATE SEQUENCE ${SCHEMA}.${NOM_TABLE}_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 999999 CACHE 1;"  > /dev/null 2>&1
fi

#echo "INFO: Déconnexion de la base de données"
#db.connect -d # --quiet

# Si l'image à traiter ($file) est dans la liste à exclure, alors on remonte en haut de la boucle pour passer à l'image suivante
cmpt=$(echo $EXCLUDE_LIST | grep $NOM_SCAN |wc -l)
if [ $cmpt -gt 0 ]; then
  echo "INFO: image $NOM_SCAN non traitée car présente dans la liste des exclusions:  $EXCLUDE_LIST"
  continue
fi

# on fait le traitement si IMAGE_UNIQUE n'est pas défini ou si l'image à traiter est celle définie dans la variable IMAGE_UNIQUE
# Pas de else

if [ ! -z "$IMAGE_UNIQUE" ]; then
   NBIMG=1
fi

if [  -z "$IMAGE_UNIQUE" -o "$(basename $file)" == "$IMAGE_UNIQUE" ]; then
echo "####################################################################"
echo "######  Traitement image: $(basename $file) (${imgNB} / ${NBIMG})########"
echo "######  Résultats dans la bdd: ${SCHEMA}.$NOM_TABLE  #######"
echo "####################################################################"

# mémorisation instant de début de la boucle
debut=$(date +%s)

if [ $DEBUG == 1 -o $DISPLAY_GRASS == 1 ]; then
 echo "INFO: démarrage de l'affichage Grass"
 d.mon stop=wx0
 d.mon start=wx0
fi

# Par défaut, on controle la surface de l étalon dans chaque image. désactivé avec l option "-ne"
if [ $ETALON == 1 ]; then
   echo "INFO: Controle surface étalon"
   source $MODULES_DIR/surfaces_etalon.sh 
   ETALON_THEORIQUE=149000   # cette valeur a été déterminée manuellement avec les réglages "prévus" pour le scanner officiel URFM
   if [ -z "$AREA_ETALON" ]; then
      AREA_ETALON=0
      echo "ERREUR: la surface de l'étalon n'est pas conforme"
      echo "Valeur théorique: $ETALON_THEORIQUE, valeur calculée: $AREA_ETALON"
      echo "Chaine de sortie de la commande d'analyse: $STRING_ETALON"
      read -r -p "Souhaitez vous poursuivre (o) ou interrompre la procédure (n) ?" poursuivre
      if [ "$poursuivre" == "${poursuivre#[oOYy]}" ]; then
        F_exit_history "INFO: Arrêt du script demandé par l'utilisateur (étalon absent)"
      fi
   fi
   diff=$(echo "$AREA_ETALON - $ETALON_THEORIQUE" | bc )
   echo "Différence: $diff"
   absdiff=${diff#-}   # on prend la valeur absolue de la différence de surface
   intdiff=$( echo $absdiff | sed -e 's/\([0-9][0-9]*\).[0-9][0-9]*/\1/')
   if [ $intdiff -gt 5000 ]; then
     echo "ERREUR: la surface de l'étalon n'est pas conforme"
     echo "Valeur théorique: $ETALON_THEORIQUE, valeur calculée: $AREA_ETALON"
     echo "Chaine de sortie de la commande d'analyse: $STRING_ETALON"
     read -r -p "Souhaitez vous poursuivre (o) ou interrompre la procédure (n) ?" poursuivre
     if [ "$poursuivre" == "${poursuivre#[oOYy]}" ]; then
        F_exit_history "INFO: Arrêt du script demandé par l'utilisateur (étalon non conforme)"
     fi
   else
    echo "INFO: Controle étalon conforme $AREA_ETALON , $ETALON_THEORIQUE, $intdiff"
   fi
   if [ $ONLY_ETALON == 1 ]; then
     # si mode only_etalon, on remonte en haut de la boucle pour passer à l'image suivante
     continue
   fi
  else
    # surface =0 dans le  cas on l'utilisateur désactive le controle de la surface de l'étalon
    AREA_ETALON=0
fi

# On définit la région d'étude
echo "INFO: définition de la zone de travail sur le scan (paramétrable)"
if [ $DEBUG == 1 ]; then
  g.region  --overwrite $DEBUGGREGION save=region --quiet   # Region restreinte en mode debug, si définie dans le fichier paramètre
  echo "INFO: Région utilisée $DEBUGGREGION"
else
    g.region  --overwrite $GREGIONS save=region  --quiet   # Région normale
    echo "INFO: Région utilisée $GREGIONS"
fi

# on applique la région d'étude
g.region region=region@$MAPSET --quiet

if [ $DEBUG == 1 -o $DISPLAY_GRASS == 1 ]; then
 echo "INFO: démarrage de l'affichage Grass"
 d.mon stop=wx0 --quiet
 d.mon start=wx0 --quiet
fi

echo "INFO: Import du scan dans GRASS"
r.in.gdal input=$file output=scan --quiet -o --overwrite

if [ $NOBLUE == 1 ]; then
  echo "INFO: ajout de 1 au canal bleu pour eviter les valeurs 0 (végétal abimé) qui bloquent le calcul FACULTATIF"
  r.mapcalc --overwrite expression=scan.blue="scan.blue@$MAPSET + 1" --quiet
fi

echo "INFO: combinaison des 3 canaux pour la composition colorée"
r.composite --overwrite red=scan.red@$MAPSET green=scan.green@$MAPSET blue=scan.blue@$MAPSET output=$NOM_SCAN --quiet

if [ $DISPLAY_GRASS  == 1 ]; then
  d.rast $NOM_SCAN@$MAPSET --quiet
fi

if [ "$ALGO" == "graines" ]; then
  # Algorithme pour les graines
  #F_balance_des_blancs    # fonction définie dans MODULES/surfaces_tools.sh
  #r.mapcalc "S =  if(scan_rescaled > $POIDS_ROUGE, 0, 1)" --overwrite 
  F_algo_graines 
else
  # Algorithme pour les feuilles
  echo "INFO: algo qui permet de trouver les pixels de végetal"
  echo "INFO: Lecture poids_bleu avant lancement algo_feuilles: $POIDS_BLEU"
  F_algo_feuilles
  echo "INFO: Lecture poids_bleu après lancement algo_feuilles: $POIDS_BLEU"
fi


if [ $DEBUG == 1 ]; then
  # on ne traite qu'une image et on sort de la boucle
  echo "INFO: sortie de la boucle (mode debug actif)"
  break
fi

if [ $MENAGE == 1 ]; then
  g.remove --quiet -f type=all pattern="scan*, S"

fi

imgNB=$((imgNB + 1))  # Incrément compteur d'images

echo "-------------------------------------------------------------------------"
echo "######  image $(basename $file) traitée en $DUREE mn"
echo "-------------------------------------------------------------------------"

fi
done  # Sortie de la boucle de traitement des images

d.mon --quiet stop=wx0

F_exit_history "Fin normale"
